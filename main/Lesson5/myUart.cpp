#include "myUart.hpp"
#include "driver/uart.h"
#include "string.h"
#include "esp_log.h"

#define TAG "UART"
#define TX_PIN 4
#define RX_PIN 5
#define RX_BUFFER_SIZE 1024
#define TX_BUFFER_SIZE 1024
#define PATTERN_LEN 3

QueueHandle_t uart_queue;

void uart_event_task(void *params){
    uart_event_t uart_event;
    uint8_t *reciever_buffer = (uint8_t*)malloc((size_t)RX_BUFFER_SIZE);
    size_t datalen;

    while (true)
    {
        if(xQueueReceive(uart_queue,&uart_event,portMAX_DELAY)){
            switch (uart_event.type)
            {
                case UART_DATA:{
                    ESP_LOGI(TAG,"UART_DATA");
                    uart_read_bytes(UART_NUM_1,reciever_buffer,uart_event.size,portMAX_DELAY);
                    printf("recieved: size %d and data %s\n",uart_event.size,reciever_buffer);
                }
                    break;
                case UART_BREAK:
                    ESP_LOGI(TAG,"UART_BREAK");
                    break;
                case UART_BUFFER_FULL:
                    ESP_LOGI(TAG,"UART_BUFFER_FULL");
                    break;
                case UART_FIFO_OVF:
                    ESP_LOGI(TAG,"UART_FIFO_OVF");
                    uart_flush_input(UART_NUM_1);
                    xQueueReset(uart_queue);
                    break;
                case UART_FRAME_ERR:
                    ESP_LOGI(TAG,"UART_FRAME_ERR");
                    break;
                case UART_PARITY_ERR:
                    ESP_LOGI(TAG,"UART_PARITY_ERR");
                    break;
                case UART_DATA_BREAK:
                    ESP_LOGI(TAG,"UART_DATA_BREAK");
                    break;
                case UART_EVENT_MAX:
                    ESP_LOGI(TAG,"UART_EVENT_MAX");
                    break;
                case UART_PATTERN_DET:{
                    ESP_LOGI(TAG,"UART_PATTERN_DET");
                    uart_get_buffered_data_len(UART_NUM_1,&datalen);
                    int pos= uart_pattern_pop_pos(UART_NUM_1);
                    ESP_LOGI(TAG,"Detected %d pos %d",datalen,pos);
                    uart_read_bytes(UART_NUM_1,reciever_buffer,datalen-PATTERN_LEN,portMAX_DELAY);
                    uint8_t pat[PATTERN_LEN+1];
                    memset(pat,0,sizeof(pat));
                    uart_read_bytes(UART_NUM_1,pat,PATTERN_LEN,pdMS_TO_TICKS(100));
                    printf("data: %.*s pattern %s\n",datalen-PATTERN_LEN,reciever_buffer,pat);
                }
                    break;
                default:
                    ESP_LOGI(TAG,"You are in default");
                    break;
            }
        }
    }
    
}



void runUart(){

    uart_config_t uart_config={
        .baud_rate = 9600,              /*!< UART baud rate*/
        .data_bits = UART_DATA_8_BITS,     /*!< UART byte size*/
        .parity    = UART_PARITY_DISABLE,          /*!< UART parity mode*/
        .stop_bits = UART_STOP_BITS_1,      /*!< UART stop bits*/
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE  /*!< UART HW flow control mode (cts/rts)*/
           /*!< UART HW RTS threshold*/
             /*!< UART source clock selection */
    };

    uart_param_config(UART_NUM_1,&uart_config);
    uart_set_pin(UART_NUM_1,TX_PIN,RX_PIN,UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM_1,RX_BUFFER_SIZE,TX_BUFFER_SIZE,20,&uart_queue,1);
    
    uart_enable_pattern_det_intr(UART_NUM_1,'+',PATTERN_LEN,10000,10,10);
    uart_pattern_queue_reset(UART_NUM_1,20);
    xTaskCreate(uart_event_task,"uart_event_task",2048,NULL,10,NULL);


/*
    char message[] = "ping send";
    printf("sending: %s\n",message);
    uart_write_bytes(UART_NUM_1,message,sizeof(message));

    char incoming_message[RX_BUFFER_SIZE];
    while (true)
    {      
        memset(incoming_message,0,sizeof(incoming_message));
        uart_read_bytes(UART_NUM_1,(uint8_t *)incoming_message,RX_BUFFER_SIZE,pdMS_TO_TICKS(500));
        printf("recieved: %s\n",incoming_message);
    }
*/
}

