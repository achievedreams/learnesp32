
#include <stdio.h>
#include <string>
using namespace std;



class Register
{
private:
    uint nameEnumIndex;
    uint8_t regAddress;
    uint8_t size;
    uint8_t startingBit;
    uint8_t mask;
    uint8_t value;

	
public:
    static uint16_t combineTo16(Register lsb,Register msb);
    static uint32_t combineTo32(Register xlsb,Register lsb,Register msb);
    static uint8_t prepareReadRegValue(Register reg);
    static uint8_t prepareSetRegValue(Register reg,uint8_t initialValue);

    Register(/* args */);
    Register(uint nameEnumIndex,uint8_t regAddress,uint8_t size,uint8_t startingBit,uint8_t mask,uint8_t value);
    ~Register();

    uint getNameEnumIndex();
    void setNameEnumIndex(uint nameEnumIndex);
    uint8_t getRegAddress();
    void setRegAddress(uint8_t regAddress);
    uint8_t getSize();
    void setSize(uint8_t size);
    uint8_t getStartingBit();
    void setStartingBit(uint8_t startingBit);
    uint8_t getMask();
    void setMask(uint8_t mask);
    uint8_t getValue();
	void setValue(uint8_t value);
};


