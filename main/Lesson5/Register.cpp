
#include "Register.hpp"



uint8_t Register::prepareReadRegValue(Register reg){
    return (reg.getValue() & reg.getMask())>>reg.getStartingBit();
}

uint8_t Register::prepareSetRegValue(Register reg,uint8_t initialValue){
    
    uint8_t maskedValue = initialValue & (reg.getMask()^0xFF);
    uint8_t result = maskedValue | (reg.getValue() <<reg.getStartingBit());
    // printf("initialValue: %X\t",initialValue);
    // printf("mask: %X\t",reg.getMask());
    // printf("maskTongle: %X\t",reg.getMask()^0xFF);
    // printf("maskedValue: %X\n",maskedValue);
    // printf("shift: %X\t",reg.getStartingBit());
    // printf("regvalue: %X\t",reg.getValue());
    // printf("shifted: %X\t",reg.getValue() <<reg.getStartingBit());
    // printf("result: %X\n",result);
    return result;
    
}

uint16_t Register::combineTo16(Register lsbR, Register msbR)
{
    uint8_t indexMsb = 0;
    uint16_t result;

    //place lsb
    uint8_t lsb = prepareReadRegValue(lsbR);
    uint8_t msb = prepareReadRegValue(msbR);
    indexMsb = lsbR.getSize();
    result=(uint16_t)(msb<<indexMsb | lsb);
    return result;
}

uint32_t Register::combineTo32(Register xlsbR, Register lsbR, Register msbR)
{
    uint8_t indexMsb=0;
    uint8_t indexLsb=0;
    uint32_t result;

    uint8_t xlsb= prepareReadRegValue(xlsbR);
    uint8_t lsb= prepareReadRegValue(lsbR);
    uint8_t msb= prepareReadRegValue(msbR);
    indexLsb=xlsbR.getSize();
    indexMsb = indexLsb + lsbR.getSize();
 
    result= (uint32_t)(msb<<indexMsb | lsb<<indexLsb | xlsb);

    return result;
}


Register::Register(/* args */)
{
}

Register::~Register()
{
}

Register::Register(uint nameEnumIndex, uint8_t regAddress, uint8_t size, uint8_t startingBit, uint8_t mask,uint8_t value)
{
    this->nameEnumIndex = nameEnumIndex;
    this->regAddress = regAddress;
    this->size = size;
    this->startingBit = startingBit;
    this->mask = mask;
    this->value=value;
}



uint Register::getNameEnumIndex()
{
    return this->nameEnumIndex;
}

void Register::setNameEnumIndex(uint nameEnumIndex)
{
    this->nameEnumIndex = nameEnumIndex;
}

uint8_t Register::getRegAddress()
{
    return this->regAddress;
}

void Register::setRegAddress(uint8_t regAddress)
{
    this->regAddress = regAddress;
}

uint8_t Register::getSize()
{
    return this->size;
}

void Register::setSize(uint8_t size)
{
    this->size = size;
}

uint8_t Register::getStartingBit()
{
    return this->startingBit;
}

void Register::setStartingBit(uint8_t startingBit)
{
    this->startingBit = startingBit;
}

uint8_t Register::getMask()
{
    return this->mask;
}

void Register::setMask(uint8_t mask)
{
    this->mask = mask;
}

uint8_t Register::getValue()
{
    return this->value;
}

void Register::setValue(uint8_t value)
{
    this->value = value;
}