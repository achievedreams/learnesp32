#include "BME680.hpp"
#include <string>
#include <stdio.h>
#include "driver/i2c.h"
#include "esp_types.h"
#include "math.h"
#include "esp_system.h"

using namespace std;

BME680::BME680()
{
}

BME680::~BME680()
{
}

void BME680::init(uint8_t sda, uint8_t clk, bool sdo, bool csb)
{
        this->_sda = sda;
        this->_clk = clk;
        this->_sdo = sdo;
        this->_csb = csb;
        if (sdo){
                this->bme680Address = _bme680Address1;
        }
        else{
                this->bme680Address = _bme680Address0;
        }

        if (csb){
                //setup I2C
                i2c_config_t i2c_config = {
                    .mode = I2C_MODE_MASTER,
                    .sda_io_num = _sda,
                    .scl_io_num = _clk,
                    .sda_pullup_en = GPIO_PULLUP_ENABLE,
                    .scl_pullup_en = GPIO_PULLUP_ENABLE,
                    .master={.clk_speed = 100000},
                    .clk_flags=0
                };
                i2c_param_config(I2C_NUM_0, &i2c_config);
                i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0);
        }
        else{
                //setup SPI
        }

        readParams();
        uint8_t chipId=readReg(this->registerMap.at(RegNames::CHIP_ID_REG));
        printf("ChipId is: %X\n",chipId);
}

void BME680::readParams()
{
        map<uint, Register>::iterator it=paramsMap.begin();

        while(it!=paramsMap.end()){
                // printf("key: %s\n", getRegNames( (RegNames)(it->first)).c_str() );
                // printf("address: %X\n",it->second.getRegAddress());
                // printf("value before: %X\n",it->second.getValue());
                it->second.setValue(readReg(it->second));
                // printf("value after: %X\n",it->second.getValue());
                it++;
        }
        // temperature params
        this->_parT1 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_T01L_REG), this->paramsMap.at(this->RegNames::PAR_T01M_REG));
        this->_parT2 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_T02L_REG), this->paramsMap.at(this->RegNames::PAR_T02M_REG));
        this->_parT3 = this->paramsMap.at(this->RegNames::PAR_T03M_REG).getValue();
        // pressure params
        this->_parP1 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_P01L_REG), this->paramsMap.at(this->RegNames::PAR_P01M_REG));
        this->_parP2 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_P02L_REG), this->paramsMap.at(this->RegNames::PAR_P02M_REG));
        this->_parP3 = this->paramsMap.at(this->RegNames::PAR_P03M_REG).getValue();
        this->_parP4 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_P04L_REG), this->paramsMap.at(this->RegNames::PAR_P04M_REG));
        this->_parP5 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_P05L_REG), this->paramsMap.at(this->RegNames::PAR_P05M_REG));
        this->_parP6 = this->paramsMap.at(this->RegNames::PAR_P06M_REG).getValue();
        this->_parP7 = this->paramsMap.at(this->RegNames::PAR_P07M_REG).getValue();
        this->_parP8 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_P08L_REG), this->paramsMap.at(this->RegNames::PAR_P08M_REG));
        this->_parP9 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_P09L_REG), this->paramsMap.at(this->RegNames::PAR_P09M_REG));
        this->_parP10 = this->paramsMap.at(this->RegNames::PAR_P10M_REG).getValue();
        //humidity params
        this->_parH1 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_H01L_REG), this->paramsMap.at(this->RegNames::PAR_H01M_REG));
        this->_parH2 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_H02L_REG), this->paramsMap.at(this->RegNames::PAR_H02M_REG));
        this->_parH3 = this->paramsMap.at(this->RegNames::PAR_H03M_REG).getValue();
        this->_parH4 = this->paramsMap.at(this->RegNames::PAR_H04M_REG).getValue();
        this->_parH5 = this->paramsMap.at(this->RegNames::PAR_H05M_REG).getValue();
        this->_parH6 = this->paramsMap.at(this->RegNames::PAR_H06M_REG).getValue();
        this->_parH7 = this->paramsMap.at(this->RegNames::PAR_H07M_REG).getValue();
        //gas params
        this->_parG1 = this->paramsMap.at(this->RegNames::PAR_G01M_REG).getValue();
        this->_parG2 = Register::combineTo16(this->paramsMap.at(this->RegNames::PAR_G02L_REG), this->paramsMap.at(this->RegNames::PAR_G02M_REG));
        this->_parG3 = this->paramsMap.at(this->RegNames::PAR_G03M_REG).getValue();
        this->_resHeatRange = Register::prepareReadRegValue( this->paramsMap.at(this->RegNames::GAS_HEATER_RESISTANSE_RANGE_REG));
        this->_resHeatVal   = this->paramsMap.at(this->RegNames::GAS_HEATER_RESISTANSE_VALUE_REG).getValue();
       
        // printf("parT1: %X\n",this->_parT1);
        // printf("parT2: %X\n",this->_parT2);
        // printf("parT3: %X\n",this->_parT3);
}

uint8_t BME680::readReg(Register reg)
{
        uint8_t result=255;

        i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();

        i2c_master_start(cmd_handle);
        i2c_master_write_byte(cmd_handle, (this->bme680Address << 1) | I2C_MASTER_WRITE, true);
        i2c_master_write_byte(cmd_handle, reg.getRegAddress(), true);
        
        i2c_master_start(cmd_handle);
        i2c_master_write_byte(cmd_handle, (this->bme680Address << 1) | I2C_MASTER_READ, true);
        i2c_master_read_byte(cmd_handle, (uint8_t *)&result, I2C_MASTER_NACK);
        i2c_master_stop(cmd_handle);
        i2c_master_cmd_begin(I2C_NUM_0, cmd_handle, 1000 / portTICK_PERIOD_MS);
        i2c_cmd_link_delete(cmd_handle);

        return result;
}

uint8_t BME680::setReg(Register reg)
{
        uint8_t valueToSet = Register::prepareSetRegValue(reg,readReg(reg));
        //printf("valueToSet: %x\n",valueToSet);
        i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();

        i2c_master_start(cmd_handle);
        i2c_master_write_byte(cmd_handle, (this->bme680Address<<1) | I2C_MASTER_WRITE, true);
        i2c_master_write_byte(cmd_handle, reg.getRegAddress(), true);
        i2c_master_write_byte(cmd_handle, valueToSet, true);
        i2c_master_stop(cmd_handle);
        i2c_master_cmd_begin(I2C_NUM_0, cmd_handle, 1000 / portTICK_PERIOD_MS);
        i2c_cmd_link_delete(cmd_handle);

        return readReg(reg);
}

void BME680::readTempADC(){
        
        this->registerMap.at(this->RegNames::TEMP_XLSB_REG).setValue(readReg(this->registerMap.at(this->RegNames::TEMP_XLSB_REG)));
        this->registerMap.at(this->RegNames::TEMP_LSB_REG ).setValue(readReg(this->registerMap.at(this->RegNames::TEMP_LSB_REG)));
        this->registerMap.at(this->RegNames::TEMP_MSB_REG ).setValue(readReg(this->registerMap.at(this->RegNames::TEMP_MSB_REG)));

        Register xlsb= this->registerMap.at(this->RegNames::TEMP_XLSB_REG);
        Register lsb = this->registerMap.at(this->RegNames::TEMP_LSB_REG);
        Register msb = this->registerMap.at(this->RegNames::TEMP_MSB_REG);


        this->_tempADC = Register::combineTo32(xlsb,lsb,msb);
}

void BME680::readTemp(){

        readTempADC();

        int64_t var1 = ((int32_t)this->_tempADC >> 3 ) - ((int32_t) this->_parT1 << 1);
        int64_t var2 = (var1 * (int32_t) this->_parT2) >> 11;
        int64_t var3 = ((((var1 >> 1) * (var1 >> 1 )) >> 12 ) * ((int32_t)this->_parT3 << 4)) >> 14; 
        this->_t_fine = var2 + var3;
        this->temperature = (double)(((this->_t_fine * 5) + 128) >> 8)/100.0 ;
}

void BME680::readPressureADC(){

        this->registerMap.at(this->RegNames::PRESS_XLSB_REG).setValue(readReg(this->registerMap.at(this->RegNames::PRESS_XLSB_REG)));
        this->registerMap.at(this->RegNames::PRESS_LSB_REG ).setValue(readReg(this->registerMap.at(this->RegNames::PRESS_LSB_REG)));
        this->registerMap.at(this->RegNames::PRESS_MSB_REG ).setValue(readReg(this->registerMap.at(this->RegNames::PRESS_MSB_REG)));

        Register xlsb= this->registerMap.at(this->RegNames::PRESS_XLSB_REG);
        Register lsb = this->registerMap.at(this->RegNames::PRESS_LSB_REG);
        Register msb = this->registerMap.at(this->RegNames::PRESS_MSB_REG);

    //    printf("press xlsb: %x,",xlsb.getValue());
    //    printf("press lsb: %x,",lsb.getValue());
    //    printf("press mlsb: %x,",msb.getValue());

        this->_pressADC= Register::combineTo32(xlsb,lsb,msb);
    //    printf("press adc: %x",this->_pressADC);
}


void BME680::readPressure(){

        readPressureADC();
        int64_t pressOnCalc;
        int64_t var1 = ((int32_t)this->_t_fine >> 1) - 64000;
        int64_t var2 = ((((var1 >> 2 ) * (var1 >> 2)) >> 11) * (int32_t)this->_parP6) >> 2 ;
        var2 = var2 + ((var1 * (int32_t)this->_parP5) << 1);
        var2 = (var2 >> 2 ) + ((int32_t)this->_parP4 << 16);
        var1 = (((((var1 >> 2) * (var1 >> 2 )) >> 13) * ((int32_t)this->_parP3 << 5)) >> 3) + (((int32_t)this->_parP2 * var1) >> 1);
        var1 = var1 >> 18;
        var1 = ((32768 + var1) * (int32_t)this->_parP1) >> 15;
        pressOnCalc = 1048576 - this->_pressADC;
        pressOnCalc = (uint32_t)((pressOnCalc - (var2 >> 12)) * ((uint32_t)3125));
        if ( pressOnCalc >= (1<<30)){
                pressOnCalc = ((pressOnCalc / (uint32_t)var1) << 1);
        }else{
                pressOnCalc = ((pressOnCalc << 1) / (uint32_t)var1);
        }
        var1 = ((int32_t)this->_parP9 * (int32_t)(((pressOnCalc >> 3) * (pressOnCalc >> 3))>>13)) >> 12;
        var2 = ((int32_t)(pressOnCalc >> 2 ) *(int32_t)this->_parP8)>> 13;
        int64_t var3 = ((int32_t)(pressOnCalc >> 8) * (int32_t)(pressOnCalc>> 8 ) * (int32_t)(pressOnCalc >> 8) * (int32_t)this->_parP10) >> 17;
        this->pressure = (double)((int32_t)(pressOnCalc) + (( var1 + var2 + var3 + ((int32_t)this->_parP7 << 7 )) >> 4))/100.0;

}

void BME680::readHummidityADC(){


        this->registerMap.at(this->RegNames::HUM_MSB_REG).setValue(readReg(this->registerMap.at(this->RegNames::HUM_MSB_REG)));
        this->registerMap.at(this->RegNames::HUM_LSB_REG).setValue(readReg(this->registerMap.at(this->RegNames::HUM_LSB_REG)));

        Register msb =this->registerMap.at(this->RegNames::HUM_MSB_REG);
        Register lsb =this->registerMap.at(this->RegNames::HUM_LSB_REG);

            //    printf("press xlsb: %x,",xlsb.getValue());
//        printf("hum lsb: %x\n",lsb.getValue());
//        printf("hum msb: %x\n",msb.getValue());
//        printf("hum adc bef: %x\n",this->_humADC);
        this->_humADC = Register::combineTo16(lsb,msb);
        // printf("hum adc aft: %x\n",this->_humADC);
}

void BME680::readHummidity(){

        readHummidityADC();
        int32_t temp_scaled = (((int32_t)this->_t_fine * 5) + 128) >> 8;
        int humOnCalc ;
        int64_t var1 = (int32_t)(this->_humADC - ((int32_t)((int32_t)this->_parH1 << 4))) - (((temp_scaled * (int32_t)this->_parH3) / ((int32_t)100)) >> 1);
        int64_t var2 = ((int32_t)this->_parH2 * (((temp_scaled * (int32_t)this->_parH4) / ((int32_t)100)) + (((temp_scaled * (( temp_scaled * (int32_t)this->_parH5) / ((int32_t)100))) >> 6) / ((int32_t)100)) + ((int32_t)(1<<14)))) >> 10;
        int64_t var3 = var1 * var2;
        int64_t var4 = (((int32_t)this->_parH6 << 7 ) + ((temp_scaled * (int32_t)this->_parH7) / ((int32_t)100))) >> 4;
        int64_t var5 = ((var3 >> 14) * (var3 >> 14)) >> 10;
        int64_t var6 = (var4 * var5) >> 1 ;
        humOnCalc = (((var3 + var6) >> 10) * ((int32_t)1000))>>  12;
        this->humidity = humOnCalc /1000.0;
        if (this->humidity > 100)
                this->humidity = 100;
        else if (this->humidity < 0)
                this->humidity = 0;
}

void BME680::setOverSamplingTPH(OverSamplingTPH tempOver,OverSamplingTPH presOver,OverSamplingTPH humOver){

        this->registerMap.at(RegNames::CONTROL_OVERSAMPLING_TEMP_REG).setValue(tempOver);
        this->registerMap.at(RegNames::CONTROL_OVERSAMPLING_PRESS_REG).setValue(presOver);
        this->registerMap.at(RegNames::CONTROL_OVERSAMPLING_HUM_REG).setValue(humOver);
        setReg(this->registerMap.at(RegNames::CONTROL_OVERSAMPLING_TEMP_REG));
        setReg(this->registerMap.at(RegNames::CONTROL_OVERSAMPLING_PRESS_REG));
        setReg(this->registerMap.at(RegNames::CONTROL_OVERSAMPLING_HUM_REG));

}

void BME680::setIIRFilter(IIRFilterCoef iir){

        this->registerMap.at(RegNames::IIR_CONFIG_REG).setValue(iir);

        setReg(this->registerMap.at(RegNames::IIR_CONFIG_REG));
}


void BME680::setGasResHeatReg(RegNames regName,int targetTemp){
        
        int32_t amb_temp = (((int32_t)this->_t_fine * 5) + 128) >> 8;
        int64_t var1 = (((uint32_t)amb_temp * this->_parG3)/10) << 8 ;
        int64_t var2 = (this->_parG1 + 784) * ((((( this->_parG2 + 154009 ) * targetTemp * 5) / 100) + 3276800) / 10);
        int64_t var3 = var1 + ( var2 >> 1);
        int64_t var4 = (var3 / (this->_resHeatRange + 4));
        int64_t var5 = (131 * this->_resHeatVal) + 65536;
        int64_t res_heat_x100 = (int32_t)(((var4 / var5) - 250) * 34);
        uint8_t res_heat_x = (uint8_t)((res_heat_x100 + 50) / 100);

        this->registerMap.at(regName).setValue(res_heat_x);

        setReg(this->registerMap.at(regName));

}

void BME680::setGasWaitReg(RegNames regName,int time){

        uint8_t gasWait=0x00;
        if(time>4032){
                time=4032;
        }else if(time<0){
                time=0;
        }

        if(time%64==0 && (time/64)<=63){
                gasWait = (GSWaitMultiFactEnum::GS_MULTIPLICATION_FACT_64 <<6)| (uint8_t)(time/64);
        }else if(time%16==0 && (time/16)<=63){
                gasWait = (GSWaitMultiFactEnum::GS_MULTIPLICATION_FACT_16 <<6) | (uint8_t)(time/16);
        }else if(time%4==0 && (time/4)<=63){
                gasWait = (GSWaitMultiFactEnum::GS_MULTIPLICATION_FACT_4 <<6)| (uint8_t)(time/4);
        }else{
                gasWait = (GSWaitMultiFactEnum::GS_MULTIPLICATION_FACT_1 <<6)| (uint8_t)(time);
        }

        this->registerMap.at(regName).setValue(gasWait);
        setReg(this->registerMap.at(regName));

}

void BME680::readGasADC(){

        this->registerMap.at(this->RegNames::GAS_RESISTANSE_MSB_REG).setValue(readReg(this->registerMap.at(this->RegNames::GAS_RESISTANSE_MSB_REG)));
        this->registerMap.at(this->RegNames::GAS_RESISTANSE_LSB_REG).setValue(readReg(this->registerMap.at(this->RegNames::GAS_RESISTANSE_LSB_REG)));

        Register msb =this->registerMap.at(this->RegNames::GAS_RESISTANSE_MSB_REG);
        Register lsb =this->registerMap.at(this->RegNames::GAS_RESISTANSE_LSB_REG);
        this->_gasADC = Register::combineTo16(lsb,msb);

}

void BME680::readGasResistance(){

        readGasADC();
        this->registerMap.at(this->RegNames::GAS_RESISTANSE_RANGE_REG).setValue(readReg(this->registerMap.at(this->RegNames::GAS_RESISTANSE_RANGE_REG)));
        this->registerMap.at(this->RegNames::GAS_ERROR_SWITCHING_REG).setValue(readReg(this->registerMap.at(this->RegNames::GAS_ERROR_SWITCHING_REG)));

        this->_gasRange = Register::prepareReadRegValue( this->registerMap.at(this->RegNames::GAS_RESISTANSE_RANGE_REG));
        this->_rangeSwitchingErr = Register::prepareReadRegValue( this->registerMap.at(this->RegNames::GAS_ERROR_SWITCHING_REG));
 
        int64_t var1 = (int64_t)(((1340 + (5 * (int64_t)this->_rangeSwitchingErr)) * ((int64_t)this->_constArray1[this->_gasRange])) >> 16);
        int64_t var2 = (int64_t)(this->_gasADC << 15) - (int64_t)(1 << 24) + var1;

        this->gasResistanse = (int32_t)((((int64_t)(this->_constArray2[this->_gasRange] * (int64_t)var1) >> 9 )+(var2 >> 1))/var2);

}


void BME680::setAltitude(){
        double seaLevel = 1013.25;
      
        this->altitude =  44330.76067 * (1.0 - pow(this->pressure / seaLevel, 0.19026));  // Convert into meters
        this->altitude = this->altitude / 100.0 ;

}

void BME680::run(OverSamplingTPH tempOver,OverSamplingTPH presOver,OverSamplingTPH humOver,
                        IIRFilterCoef iir,RegNames gasResHearRegName,int targetTemp,
                        RegNames gasWaitRegName,int gasWaitTime){

        while(this->measuring()){
                printf("in run \n");
        }

        setOverSamplingTPH(tempOver,presOver,humOver);
        setIIRFilter(iir);

         // set the profile
        this->registerMap.at(RegNames::GAS_PROFILE_CONTROL_REG).setValue(0);
        setReg(this->registerMap.at(RegNames::GAS_PROFILE_CONTROL_REG));

        setGasWaitReg(gasWaitRegName,gasWaitTime);
        setGasResHeatReg(gasResHearRegName,targetTemp);
         

        // set run gas
        this->registerMap.at(RegNames::GAS_RUN_REG).setValue(1);
        setReg(this->registerMap.at(RegNames::GAS_RUN_REG));

        this->registerMap.at(RegNames::CONTROL_MEASUREMENT_MODE_REG).setValue(1);
        setReg(this->registerMap.at(RegNames::CONTROL_MEASUREMENT_MODE_REG));

        calculateData();		

}

bool BME680::measuring(){
        this->registerMap.at(RegNames::MEASURING_STATUS_FLAG_REG).setValue(readReg(this->registerMap.at(RegNames::MEASURING_STATUS_FLAG_REG)));
        uint8_t measuring = Register::prepareReadRegValue(this->registerMap.at(RegNames::MEASURING_STATUS_FLAG_REG));
        
        if(measuring!=0){
                printf("Still measuring.\n");
                vTaskDelay(100/portTICK_PERIOD_MS);
                return true;
       }
       return false;
}

bool BME680::gasMeasuring(){
        this->registerMap.at(RegNames::GAS_MEASURING_STATUS_FLAG_REG).setValue(readReg(this->registerMap.at(RegNames::GAS_MEASURING_STATUS_FLAG_REG)));
        uint8_t gasMeasuring = Register::prepareReadRegValue(this->registerMap.at(RegNames::GAS_MEASURING_STATUS_FLAG_REG));
        
        if(gasMeasuring!=0){
                printf("Still Gas measuring.\n");  
                vTaskDelay(100/portTICK_PERIOD_MS);
                return true;
        }
        return false;
}

bool BME680::newData(){
        this->registerMap.at(RegNames::NEW_DATA_REG).setValue(readReg(this->registerMap.at(RegNames::NEW_DATA_REG)));
        uint8_t newData = Register::prepareReadRegValue(this->registerMap.at(RegNames::NEW_DATA_REG));
  
        if(newData!=0){
                printf("Waiting for new Data.\n");
                vTaskDelay(100/portTICK_PERIOD_MS);
                return false;
        }
        return true;
}

void BME680::calculateData(){
     
        bool gasFlag=true;
       
        while(!this->measuring()){}
        uint8_t measuring = Register::prepareReadRegValue(this->registerMap.at(RegNames::MEASURING_STATUS_FLAG_REG));
        printf("Measuring finished : %d\n",measuring);
        
        vTaskDelay(50/portTICK_PERIOD_MS);
        
        while(!this->gasMeasuring()){}
        uint8_t gasMeasuring = Register::prepareReadRegValue(this->registerMap.at(RegNames::GAS_MEASURING_STATUS_FLAG_REG));
        printf("Gas Measuring finished : %d\n",gasMeasuring);
        
        vTaskDelay(50/portTICK_PERIOD_MS);
        
        this->registerMap.at(RegNames::GAS_VALID_STATUS_REG).setValue(readReg(this->registerMap.at(RegNames::GAS_VALID_STATUS_REG)));
        this->registerMap.at(RegNames::HEATER_TEMP_STAB_STATUS_REG).setValue(readReg(this->registerMap.at(RegNames::HEATER_TEMP_STAB_STATUS_REG)));
        
        uint8_t gasValidMeasure = Register::prepareReadRegValue(this->registerMap.at(RegNames::GAS_VALID_STATUS_REG));
        uint8_t heaterStabStatus = Register::prepareReadRegValue(this->registerMap.at(RegNames::HEATER_TEMP_STAB_STATUS_REG));

        if(gasValidMeasure==0){
                printf("Invalid gas measure\n");
                gasFlag=false;
        }

        if(heaterStabStatus==0){
                printf("Gas heater not reached target\n");
                gasFlag=false;
        }

        while(!this->newData()){}
        uint8_t newData = Register::prepareReadRegValue(this->registerMap.at(RegNames::NEW_DATA_REG));
        printf("New Data exist : %d\n",newData);

        readTemp();
        readPressure();
        readHummidity();
        setAltitude();
        //if(gasFlag){
                readGasResistance();
        //}
        printTPHG(gasFlag);
        

}

void BME680::printTPHG(bool gasFlag){

        printf("Temperature: %.2f, ",this->temperature);
        printf("Pressure: %.2f, ",this->pressure);
        printf("Hummidity: %.2f %% ",this->humidity);
        if(gasFlag){
                printf("Gas Resistance: %d ",this->gasResistanse);
        }else{
                printf("Gas Measurement with issue. ");
                printf("Gas Resistance: %d ,",this->gasResistanse);
        }
        
        printf("Altitude: %.2f \n",this->altitude);

}

string BME680::getRegNames(RegNames regname)
{
        
        switch (regname)
        {
                case CONTROL_MEASUREMENT_MODE_REG:
                        return "CONTROL_MEASUREMENT_MODE_REG";
                        break;
                case RESET_REG:
                        return "RESET_REG";
                        break;
                case CHIP_ID_REG:
                        return "CHIP_ID_REG";
                        break;
                case CONTROL_OVERSAMPLING_HUM_REG:
                        return "CONTROL_OVERSAMPLING_HUM_REG";
                        break;
                case CONTROL_OVERSAMPLING_TEMP_REG:
                        return "CONTROL_OVERSAMPLING_TEMP_REG";
                        break;
                case CONTROL_OVERSAMPLING_PRESS_REG:
                        return "CONTROL_OVERSAMPLING_PRESS_REG";
                        break;
                case IIR_CONFIG_REG:
                        return "IIR_CONFIG_REG";
                        break;
                case GAS_RES_HEAT_REG_0:
                        return "GAS_RES_HEAT_REG_0";
                        break;
                case GAS_RES_HEAT_REG_1:
                        return "GAS_RES_HEAT_REG_1";
                        break;
                case GAS_RES_HEAT_REG_2:
                        return "GAS_RES_HEAT_REG_2";
                        break;
                case GAS_RES_HEAT_REG_3:
                        return "GAS_RES_HEAT_REG_3";
                        break;
                case GAS_RES_HEAT_REG_4:
                        return "GAS_RES_HEAT_REG_4";
                        break;
                case GAS_RES_HEAT_REG_5:
                        return "GAS_RES_HEAT_REG_5";
                        break;
                case GAS_RES_HEAT_REG_6:
                        return "GAS_RES_HEAT_REG_6";
                        break;
                case GAS_RES_HEAT_REG_7:
                        return "GAS_RES_HEAT_REG_7";
                        break;
                case GAS_RES_HEAT_REG_8:
                        return "GAS_RES_HEAT_REG_8";
                        break;
                case GAS_RES_HEAT_REG_9:
                        return "GAS_RES_HEAT_REG_9";
                        break;
                case GAS_WAIT_REG_0:
                        return "GAS_WAIT_REG_0";
                        break;
                case GAS_WAIT_REG_1:
                        return "GAS_WAIT_REG_1";
                        break;
                case GAS_WAIT_REG_2:
                        return "GAS_WAIT_REG_2";
                        break;
                case GAS_WAIT_REG_3:
                        return "GAS_WAIT_REG_3";
                        break;
                case GAS_WAIT_REG_4:
                        return "GAS_WAIT_REG_4";
                        break;
                case GAS_WAIT_REG_5:
                        return "GAS_WAIT_REG_5";
                        break;
                case GAS_WAIT_REG_6:
                        return "GAS_WAIT_REG_6";
                        break;
                case GAS_WAIT_REG_7:
                        return "GAS_WAIT_REG_7";
                        break;
                case GAS_WAIT_REG_8:
                        return "GAS_WAIT_REG_8";
                        break;
                case GAS_WAIT_REG_9:
                        return "GAS_WAIT_REG_9";
                        break;
                case GAS_CONTROL_HEATER_OFF_REG:
                        return "GAS_CONTROL_HEATER_OFF_REG";
                        break;
                case GAS_PROFILE_CONTROL_REG:
                        return "GAS_PROFILE_CONTROL_REG";
                        break;
                case GAS_RUN_REG:
                        return "GAS_RUN_REG";
                        break;
                case PRESS_MSB_REG:
                        return "PRESS_MSB_REG";
                        break;
                case PRESS_LSB_REG:
                        return "PRESS_LSB_REG";
                        break;
                case PRESS_XLSB_REG:
                        return "PRESS_XLSB_REG";
                        break;
                case TEMP_MSB_REG:
                        return "TEMP_MSB_REG";
                        break;
                case TEMP_LSB_REG:
                        return "TEMP_LSB_REG";
                        break;
                case TEMP_XLSB_REG:
                        return "TEMP_XLSB_REG";
                        break;
                case HUM_MSB_REG:
                        return "HUM_MSB_REG";
                        break;
                case HUM_LSB_REG:
                        return "HUM_LSB_REG";
                        break;
                case GAS_RESISTANSE_MSB_REG:
                        return "GAS_RESISTANSE_MSB_REG";
                        break;
                case GAS_RESISTANSE_LSB_REG:
                        return "GAS_RESISTANSE_LSB_REG";
                        break;
                case GAS_RESISTANSE_RANGE_REG:
                        return "GAS_RESISTANSE_RANGE_REG";
                        break;
                case NEW_DATA_REG:
                        return "NEW_DATA_REG";
                        break;
                case GAS_MEASURING_STATUS_FLAG_REG:
                        return "GAS_MEASURING_STATUS_FLAG_REG";
                        break;
                case MEASURING_STATUS_FLAG_REG:
                        return "MEASURING_STATUS_FLAG_REG";
                        break;
                case GAS_MEASUREMENT_INDEX_REG:
                        return "GAS_MEASUREMENT_INDEX_REG";
                        break;
                case GAS_VALID_STATUS_REG:
                        return "GAS_VALID_STATUS_REG";
                        break;
                case HEATER_TEMP_STAB_STATUS_REG:
                        return "HEATER_TEMP_STAB_STATUS_REG";
                        break;
                case PAR_T01M_REG:
                        return "PAR_T01M_REG";
                        break;
                case PAR_T01L_REG:
                        return "PAR_T01L_REG";
                        break;
                case PAR_T02M_REG:
                        return "PAR_T02M_REG";
                        break;
                case PAR_T02L_REG:
                        return "PAR_T02L_REG";
                        break;
                case PAR_T03M_REG:
                        return "PAR_T03M_REG";
                        break;
                case PAR_P01M_REG:
                        return "PAR_P01M_REG";
                        break;
                case PAR_P01L_REG:
                        return "PAR_P01L_REG";
                        break;
                case PAR_P02M_REG:
                        return "PAR_P02M_REG";
                        break;
                case PAR_P02L_REG:
                        return "PAR_P02L_REG";
                        break;
                case PAR_P03M_REG:
                        return "PAR_P03M_REG";
                        break;
                case PAR_P04M_REG:
                        return "PAR_P04M_REG";
                        break;
                case PAR_P04L_REG:
                        return "PAR_P04L_REG";
                        break;
                case PAR_P05M_REG:
                        return "PAR_P05M_REG";
                        break;
                case PAR_P05L_REG:
                        return "PAR_P05L_REG";
                        break;
                case PAR_P06M_REG:
                        return "PAR_P06M_REG";
                        break;
                case PAR_P07M_REG:
                        return "PAR_P07M_REG";
                        break;
                case PAR_P08M_REG:
                        return "PAR_P08M_REG";
                        break;
                case PAR_P08L_REG:
                        return "PAR_P08L_REG";
                        break;
                case PAR_P09M_REG:
                        return "PAR_P09M_REG";
                        break;
                case PAR_P09L_REG:
                        return "PAR_P09L_REG";
                        break;
                case PAR_P10M_REG:
                        return "PAR_P10M_REG";
                        break;
                case PAR_H01M_REG:
                        return "PAR_H01M_REG";
                        break;
                case PAR_H01L_REG:
                        return "PAR_H01L_REG";
                        break;
                case PAR_H02M_REG:
                        return "PAR_H02M_REG";
                        break;
                case PAR_H02L_REG:
                        return "PAR_H02L_REG";
                        break;
                case PAR_H03M_REG:
                        return "PAR_H03M_REG";
                        break;
                case PAR_H04M_REG:
                        return "PAR_H04M_REG";
                        break;
                case PAR_H05M_REG:
                        return "PAR_H05M_REG";
                        break;
                case PAR_H06M_REG:
                        return "PAR_H06M_REG";
                        break;
                case PAR_H07M_REG:
                        return "PAR_H07M_REG";
                        break;
                case PAR_G01M_REG:
                        return "PAR_G01M_REG";
                        break;
                case PAR_G02M_REG:
                        return "PAR_G02M_REG";
                        break;
                case PAR_G02L_REG:
                        return "PAR_G02L_REG";
                        break;
                case PAR_G03M_REG:
                        return "PAR_G03M_REG";
                        break;
                case GAS_HEATER_RESISTANSE_RANGE_REG:
                        return "GAS_HEATER_RESISTANSE_RANGE_REG";
                        break;
                case GAS_HEATER_RESISTANSE_VALUE_REG:
                        return "GAS_HEATER_RESISTANSE_VALUE_REG";
                        break;
                default:
                        return "";
        }
}
