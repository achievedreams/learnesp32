#include <stdio.h>
#include <string>
#include "esp_system.h"
#include <map>
#include "Register.hpp"
using namespace std;

class BME680  
{
	private:

// Temp params
		uint8_t _parT3;
		uint16_t _parT1, _parT2;
		uint32_t _tempADC;
		int64_t _t_fine;

// Pressure params
		uint8_t _parP3, _parP6, _parP7, _parP10; 
		uint16_t _parP1, _parP2, _parP4, _parP5, _parP8, _parP9;
		uint32_t _pressADC;
	
// Humidity params
		uint8_t _parH3, _parH4, _parH5, _parH6,  _parH7;
		uint16_t _parH1, _parH2, _humADC;

// Air quality params
		int8_t _rangeSwitchingErr;
		uint8_t _parG1, _parG3, _resHeatRange, _gasRange;
		uint16_t _parG2, _gasADC;
		int8_t _resHeatVal;

		const uint8_t _bme680Address0=0x76;
		const uint8_t _bme680Address1=0x77;
		uint8_t _sda, _clk;
		bool _sdo, _csb;
	
		const double _constArray1[16] = {
			2147483647,2147483647,2147483647,2147483647,2147483647,
			2126008810,2147483647,2130303777,2147483647,2147483647,
			2143188679,2136746228,2147483647,2126008810,2147483647,2147483647
		};

		const double _constArray2[16] = {
			4096000000,2048000000,1024000000,512000000,255744255,127110228,
			64000000,32258064,16016016,8000000,4000000,2000000,1000000,500000,250000,125000
		};
		
	public:


		typedef enum SensorModeEnum{
			SLEEP_BME680,FORCED_BME680
			} SensorMode;

		typedef enum RegNamesEnum : uint
		{
			CONTROL_MEASUREMENT_MODE_REG,
			RESET_REG,
			CHIP_ID_REG,
			CONTROL_OVERSAMPLING_HUM_REG,
			CONTROL_OVERSAMPLING_TEMP_REG,
			CONTROL_OVERSAMPLING_PRESS_REG,
			IIR_CONFIG_REG,
			GAS_RES_HEAT_REG_0,
			GAS_RES_HEAT_REG_1,
			GAS_RES_HEAT_REG_2,
			GAS_RES_HEAT_REG_3,
			GAS_RES_HEAT_REG_4,
			GAS_RES_HEAT_REG_5,
			GAS_RES_HEAT_REG_6,
			GAS_RES_HEAT_REG_7,
			GAS_RES_HEAT_REG_8,
			GAS_RES_HEAT_REG_9,
			GAS_WAIT_REG_0,
			GAS_WAIT_REG_1,
			GAS_WAIT_REG_2,
			GAS_WAIT_REG_3,
			GAS_WAIT_REG_4,
			GAS_WAIT_REG_5,
			GAS_WAIT_REG_6,
			GAS_WAIT_REG_7,
			GAS_WAIT_REG_8,
			GAS_WAIT_REG_9,
			GAS_CONTROL_HEATER_OFF_REG,
			GAS_PROFILE_CONTROL_REG,
			GAS_RUN_REG,
			PRESS_MSB_REG,
			PRESS_LSB_REG,
			PRESS_XLSB_REG,
			TEMP_MSB_REG,
			TEMP_LSB_REG,
			TEMP_XLSB_REG,
			HUM_MSB_REG,
			HUM_LSB_REG,
			GAS_RESISTANSE_MSB_REG,
			GAS_RESISTANSE_LSB_REG,
			GAS_RESISTANSE_RANGE_REG,
			NEW_DATA_REG,
			GAS_MEASURING_STATUS_FLAG_REG,
			MEASURING_STATUS_FLAG_REG,
			GAS_MEASUREMENT_INDEX_REG,
			GAS_VALID_STATUS_REG,
			HEATER_TEMP_STAB_STATUS_REG,
			GAS_HEATER_RESISTANSE_RANGE_REG,
			GAS_HEATER_RESISTANSE_VALUE_REG,
			GAS_ERROR_SWITCHING_REG,
			


			// params regs
			PAR_T01M_REG,
			PAR_T01L_REG,
			PAR_T02M_REG,
			PAR_T02L_REG,
			PAR_T03M_REG,
			PAR_P01M_REG,
			PAR_P01L_REG,
			PAR_P02M_REG,
			PAR_P02L_REG,
			PAR_P03M_REG,
			PAR_P04M_REG,
			PAR_P04L_REG,
			PAR_P05M_REG,
			PAR_P05L_REG,
			PAR_P06M_REG,
			PAR_P07M_REG,
			PAR_P08M_REG,
			PAR_P08L_REG,
			PAR_P09M_REG,
			PAR_P09L_REG,
			PAR_P10M_REG,
			PAR_H01M_REG,
			PAR_H01L_REG,
			PAR_H02M_REG,
			PAR_H02L_REG,
			PAR_H03M_REG,
			PAR_H04M_REG,
			PAR_H05M_REG,
			PAR_H06M_REG,
			PAR_H07M_REG,
			PAR_G01M_REG,
			PAR_G02M_REG,
			PAR_G02L_REG,
			PAR_G03M_REG
		} RegNames;

		typedef enum OverSamplingTPHEnum {
			SKIPPED, // 000
			OVERSAMPLING_X1, // 001
			OVERSAMPLING_X2, // 010
			OVERSAMPLING_X4, // 011
			OVERSAMPLING_X8, // 100
			OVERSAMPLING_X16 // 101
		}OverSamplingTPH;

		typedef enum IIRFilterCoefEnum {
			IIR_FILTER_COEF_0,
			IIR_FILTER_COEF_1,
			IIR_FILTER_COEF_3,
			IIR_FILTER_COEF_7,
			IIR_FILTER_COEF_15,
			IIR_FILTER_COEF_31,
			IIR_FILTER_COEF_63,
			IIR_FILTER_COEF_127
		}IIRFilterCoef;

		typedef enum GSWaitMultiFactEnum{
			GS_MULTIPLICATION_FACT_1,
			GS_MULTIPLICATION_FACT_4,
			GS_MULTIPLICATION_FACT_16,
			GS_MULTIPLICATION_FACT_64
		}GSWaitMultiFactor;

		struct MasksEnum{
			uint8_t full = 0xFF;
			uint8_t zeroSizeTwo = 0X03;
			uint8_t zeroSizeThree = 0X07;
			uint8_t zeroSizeFour = 0x0F;
			uint8_t twoSizeThree = 0x1C;
			uint8_t threeAlone = 0x08;
			uint8_t fourAlone = 0x10;
			uint8_t fourSizeTwo = 0x30;
			uint8_t fourSizeFour = 0xF0;
			uint8_t fiveAlone = 0x20;
			uint8_t fiveSizeThree = 0xE0;
			uint8_t sixAlone= 0x40;
			uint8_t sixSizeTow= 0xC0;
			uint8_t sevenAlone= 0x80;
			
		}mask;

		map<uint, Register> registerMap= { 
			{RegNames::CONTROL_MEASUREMENT_MODE_REG, Register(RegNames::CONTROL_MEASUREMENT_MODE_REG,0x74,2,0,mask.zeroSizeTwo,255)},
			{RegNames::RESET_REG,Register(RegNames::RESET_REG,0xE0,8,0,mask.full,255)}, // reset with value 0xB6
			{RegNames::CHIP_ID_REG,Register(RegNames::CHIP_ID_REG,0xD0,8,0,mask.full,255)},
			{RegNames::CONTROL_OVERSAMPLING_HUM_REG,Register(RegNames::CONTROL_OVERSAMPLING_HUM_REG,0x72,3,0,mask.zeroSizeThree,255)},
			{RegNames::CONTROL_OVERSAMPLING_TEMP_REG,Register(RegNames::CONTROL_OVERSAMPLING_TEMP_REG,0x74,3,5,mask.fiveSizeThree,255)},
			{RegNames::CONTROL_OVERSAMPLING_PRESS_REG,Register(RegNames::CONTROL_OVERSAMPLING_PRESS_REG,0x74,3,2,mask.twoSizeThree,255)},
			{RegNames::IIR_CONFIG_REG,Register(RegNames::IIR_CONFIG_REG,0x75,3,2,mask.twoSizeThree,255)},
			{RegNames::GAS_RES_HEAT_REG_0,Register(RegNames::GAS_RES_HEAT_REG_0,0x5A,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_1,Register(RegNames::GAS_RES_HEAT_REG_1,0x5B,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_2,Register(RegNames::GAS_RES_HEAT_REG_2,0x5C,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_3,Register(RegNames::GAS_RES_HEAT_REG_3,0x5D,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_4,Register(RegNames::GAS_RES_HEAT_REG_4,0x5E,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_5,Register(RegNames::GAS_RES_HEAT_REG_5,0x5F,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_6,Register(RegNames::GAS_RES_HEAT_REG_6,0x60,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_7,Register(RegNames::GAS_RES_HEAT_REG_7,0x61,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_8,Register(RegNames::GAS_RES_HEAT_REG_8,0x62,8,0,mask.full,255)},
			{RegNames::GAS_RES_HEAT_REG_9,Register(RegNames::GAS_RES_HEAT_REG_9,0x63,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_0,Register(RegNames::GAS_WAIT_REG_0,0x64,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_1,Register(RegNames::GAS_WAIT_REG_1,0x65,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_2,Register(RegNames::GAS_WAIT_REG_2,0x66,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_3,Register(RegNames::GAS_WAIT_REG_3,0x67,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_4,Register(RegNames::GAS_WAIT_REG_4,0x68,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_5,Register(RegNames::GAS_WAIT_REG_5,0x69,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_6,Register(RegNames::GAS_WAIT_REG_6,0x6A,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_7,Register(RegNames::GAS_WAIT_REG_7,0x6B,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_8,Register(RegNames::GAS_WAIT_REG_8,0x6C,8,0,mask.full,255)},
			{RegNames::GAS_WAIT_REG_9,Register(RegNames::GAS_WAIT_REG_9,0x6D,8,0,mask.full,255)},
			{RegNames::GAS_CONTROL_HEATER_OFF_REG,Register(RegNames::GAS_CONTROL_HEATER_OFF_REG,0x70,1,3,mask.threeAlone,255)},
			{RegNames::GAS_PROFILE_CONTROL_REG,Register(RegNames::GAS_PROFILE_CONTROL_REG,0x71,4,0,mask.zeroSizeFour,255)},
			{RegNames::GAS_RUN_REG,Register(RegNames::GAS_RUN_REG,0x71,1,4,mask.fourAlone,255)},
			{RegNames::PRESS_MSB_REG,Register(RegNames::PRESS_MSB_REG,0x1F,8,0,mask.full,255)},
			{RegNames::PRESS_LSB_REG,Register(RegNames::PRESS_LSB_REG,0x20,8,0,mask.full,255)},
			{RegNames::PRESS_XLSB_REG,Register(RegNames::PRESS_XLSB_REG,0x21,4,4,mask.fourSizeFour,255)},
			{RegNames::TEMP_MSB_REG,Register(RegNames::TEMP_MSB_REG,0x22,8,0,mask.full,255)},
			{RegNames::TEMP_LSB_REG,Register(RegNames::TEMP_LSB_REG,0x23,8,0,mask.full,255)},
			{RegNames::TEMP_XLSB_REG,Register(RegNames::TEMP_XLSB_REG,0x24,4,4,mask.fourSizeFour,255)},
			{RegNames::HUM_MSB_REG,Register(RegNames::HUM_MSB_REG,0x25,8,0,mask.full,255)},
			{RegNames::HUM_LSB_REG,Register(RegNames::HUM_LSB_REG,0x26,8,0,mask.full,255)},
			{RegNames::GAS_RESISTANSE_MSB_REG,Register(RegNames::GAS_RESISTANSE_MSB_REG,0x2A,8,0,mask.full,255)},
			{RegNames::GAS_RESISTANSE_LSB_REG,Register(RegNames::GAS_RESISTANSE_LSB_REG,0x2B,2,6,mask.sixSizeTow,255)},
			{RegNames::GAS_RESISTANSE_RANGE_REG,Register(RegNames::GAS_RESISTANSE_RANGE_REG,0x2B,4,0,mask.zeroSizeFour,255)},
			{RegNames::GAS_ERROR_SWITCHING_REG,Register(RegNames::GAS_ERROR_SWITCHING_REG,0x04,48,0,mask.full,255)},
			{RegNames::NEW_DATA_REG,Register(RegNames::NEW_DATA_REG,0x1D,1,7,mask.sevenAlone,255)},
			{RegNames::GAS_MEASURING_STATUS_FLAG_REG,Register(RegNames::GAS_MEASURING_STATUS_FLAG_REG,0x1D,1,6,mask.sixAlone,255)},
			{RegNames::MEASURING_STATUS_FLAG_REG,Register(RegNames::MEASURING_STATUS_FLAG_REG,0x1D,1,5,mask.fiveAlone,255)},
			{RegNames::GAS_MEASUREMENT_INDEX_REG,Register(RegNames::GAS_MEASUREMENT_INDEX_REG,0x1D,4,0,mask.zeroSizeFour,255)},
			{RegNames::GAS_VALID_STATUS_REG,Register(RegNames::GAS_VALID_STATUS_REG,0x2B,1,5,mask.fiveAlone,255)},
			{RegNames::HEATER_TEMP_STAB_STATUS_REG,Register(RegNames::HEATER_TEMP_STAB_STATUS_REG,0x2B,1,4,mask.fourAlone,255)}
		
		
		};

		map<uint, Register> paramsMap={
			{RegNames::PAR_T01M_REG,Register(RegNames::PAR_T01M_REG,0xEA,8,0,mask.full,255)},
			{RegNames::PAR_T01L_REG,Register(RegNames::PAR_T01L_REG,0xE9,8,0,mask.full,255)},
			{RegNames::PAR_T02M_REG,Register(RegNames::PAR_T02M_REG,0x8B,8,0,mask.full,255)},
			{RegNames::PAR_T02L_REG,Register(RegNames::PAR_T02L_REG,0x8A,8,0,mask.full,255)},
			{RegNames::PAR_T03M_REG,Register(RegNames::PAR_T03M_REG,0x8C,8,0,mask.full,255)},
			{RegNames::PAR_P01M_REG,Register(RegNames::PAR_P01M_REG,0x8F,8,0,mask.full,255)},
			{RegNames::PAR_P01L_REG,Register(RegNames::PAR_P01L_REG,0x8E,8,0,mask.full,255)},
			{RegNames::PAR_P02M_REG,Register(RegNames::PAR_P02M_REG,0x91,8,0,mask.full,255)},
			{RegNames::PAR_P02L_REG,Register(RegNames::PAR_P02L_REG,0x90,8,0,mask.full,255)},
			{RegNames::PAR_P03M_REG,Register(RegNames::PAR_P03M_REG,0x92,8,0,mask.full,255)},
			{RegNames::PAR_P04M_REG,Register(RegNames::PAR_P04M_REG,0x95,8,0,mask.full,255)},
			{RegNames::PAR_P04L_REG,Register(RegNames::PAR_P04L_REG,0x94,8,0,mask.full,255)},
			{RegNames::PAR_P05M_REG,Register(RegNames::PAR_P05M_REG,0x97,8,0,mask.full,255)},
			{RegNames::PAR_P05L_REG,Register(RegNames::PAR_P05L_REG,0x96,8,0,mask.full,255)},
			{RegNames::PAR_P06M_REG,Register(RegNames::PAR_P06M_REG,0x99,8,0,mask.full,255)},
			{RegNames::PAR_P07M_REG,Register(RegNames::PAR_P07M_REG,0x98,8,0,mask.full,255)},
			{RegNames::PAR_P08M_REG,Register(RegNames::PAR_P08M_REG,0x9D,8,0,mask.full,255)},
			{RegNames::PAR_P08L_REG,Register(RegNames::PAR_P08L_REG,0x9C,8,0,mask.full,255)},
			{RegNames::PAR_P09M_REG,Register(RegNames::PAR_P09M_REG,0x9E,8,0,mask.full,255)},
			{RegNames::PAR_P09L_REG,Register(RegNames::PAR_P09L_REG,0x9F,8,0,mask.full,255)},
			{RegNames::PAR_P10M_REG,Register(RegNames::PAR_P10M_REG,0xA0,8,0,mask.full,255)},
			{RegNames::PAR_H01M_REG,Register(RegNames::PAR_H01M_REG,0xE3,8,0,mask.full,255)},
			{RegNames::PAR_H01L_REG,Register(RegNames::PAR_H01L_REG,0xE2,4,0,mask.zeroSizeFour,255)},
			{RegNames::PAR_H02M_REG,Register(RegNames::PAR_H02M_REG,0xE1,8,0,mask.full,255)},
			{RegNames::PAR_H02L_REG,Register(RegNames::PAR_H02L_REG,0xE2,4,4,mask.fourSizeFour,255)},
			{RegNames::PAR_H03M_REG,Register(RegNames::PAR_H03M_REG,0xE4,8,0,mask.full,255)},
			{RegNames::PAR_H04M_REG,Register(RegNames::PAR_H04M_REG,0xE5,8,0,mask.full,255)},
			{RegNames::PAR_H05M_REG,Register(RegNames::PAR_H05M_REG,0xE6,8,0,mask.full,255)},
			{RegNames::PAR_H06M_REG,Register(RegNames::PAR_H06M_REG,0xE7,8,0,mask.full,255)},
			{RegNames::PAR_H07M_REG,Register(RegNames::PAR_H07M_REG,0xE8,8,0,mask.full,255)},
			{RegNames::PAR_G01M_REG,Register(RegNames::PAR_G01M_REG,0xED,8,0,mask.full,255)},
			{RegNames::PAR_G02M_REG,Register(RegNames::PAR_G02M_REG,0xEC,8,0,mask.full,255)},
			{RegNames::PAR_G02L_REG,Register(RegNames::PAR_G02L_REG,0xEB,8,0,mask.full,255)},
			{RegNames::PAR_G03M_REG,Register(RegNames::PAR_G03M_REG,0xEE,8,0,mask.full,255)},
			{RegNames::GAS_HEATER_RESISTANSE_RANGE_REG,Register(RegNames::GAS_HEATER_RESISTANSE_RANGE_REG,0x02,2,4,mask.fourSizeTwo,255)},
			{RegNames::GAS_HEATER_RESISTANSE_VALUE_REG,Register(RegNames::GAS_HEATER_RESISTANSE_VALUE_REG,0x00,8,0,mask.full,255)}
			
			//params regs
			
		};

		uint8_t bme680Address;
		double temperature;
		double humidity;
		double pressure;
		int32_t gasResistanse;
		double altitude;


		BME680();
		~BME680();

		string getRegNames(RegNames regname);
		
		void init(uint8_t sda,uint8_t clk,bool sdo,bool csb);
		void run(OverSamplingTPH tempOver,OverSamplingTPH presOver,OverSamplingTPH humOver,IIRFilterCoef iir,RegNames gasResHearRegName,int targetTemp,RegNames gasWaitRegName,int gasWaitTime);
		void calculateData();
		void printTPHG(bool gasFlag);

		void readParams();
		void readTempADC();
		void readTemp();
		void readPressureADC();
		void readPressure();
		void readHummidityADC();
		void readHummidity();
		void readGasADC();
		void readGasResistance();

		bool measuring();
		bool gasMeasuring();
		bool newData();
		
		void setOverSamplingTPH(OverSamplingTPH tempOver,OverSamplingTPH presOver,OverSamplingTPH humOver);
		void setIIRFilter(IIRFilterCoef iir);
		void setGasResHeatReg(RegNames regName,int targetTemp);
		void setGasWaitReg(RegNames regName,int time);
		void setAltitude();
		
		
		uint8_t readReg(Register reg);
		uint8_t setReg(Register reg);
};
