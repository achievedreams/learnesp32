#include "Sdcard.hpp"

#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <sys/dirent.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "freertos/task.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"

static const char *TAG = "SD-CARD";



#define MAX_BUFFSIZE 16384


void runSd(){

    sdmmc_host_t host=SDSPI_HOST_DEFAULT();
   
    sdspi_device_config_t slot_config=SDSPI_DEVICE_CONFIG_DEFAULT();
    
    //sdspi_dev_handle_t dev_handle;
    //sdspi_host_init_device(&slot_config,&dev_handle);
    // sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
    // slot_config.gpio_miso = GPIO_NUM_19;
    // slot_config.gpio_mosi = GPIO_NUM_21;
    // slot_config.gpio_sck  = GPIO_NUM_18;
    // slot_config.gpio_cs   = GPIO_NUM_5;

    esp_vfs_fat_mount_config_t mount_config = {
        .format_if_mount_failed = false,
        .max_files = 10,
        .allocation_unit_size=16*1024
    };

    sdmmc_card_t *card;
    sdmmc_host_init();
   esp_err_t ret = esp_vfs_fat_sdspi_mount("/sdcard",&host,&slot_config,&mount_config,&card);
    if(ret!=ESP_OK){
        if(ret == ESP_FAIL){
            ESP_LOGE(TAG,"Failed to mount filesystem."
                        "If you want the card to be formatted , set format_if_mount_failed = true");
        }else{
            ESP_LOGE(TAG,"Failed to initialize the card (%d)."
                        "Make sure SD card lines have pull-up resistors in place.",ret);
        }
        return;
    }

    sdmmc_card_print_info(stdout,card);

    ESP_LOGI(TAG,"Opening File");
    FILE *f = fopen("/sdcard/hello.txt","w");
    fclose(f);
    ESP_LOGI(TAG,"File written");

    struct stat st;
    if(stat("/sdcoard/foo.txt",&st)==0){
        unlink("/sdcard/foo.txt");
    }

    ESP_LOGI(TAG,"Rename file");
    if(rename("/sdcard/hello.txt","/sdcard/foo.txt")!=0){
        ESP_LOGE(TAG,"Rename Failed");
        return;
    }

    ESP_LOGI(TAG,"Reading file");
    f = fopen("/sdcard/foo.txt","r");
    if(f== NULL){
        ESP_LOGE(TAG,"Failed to open file for reading");
        return ;
    }
    char line[64];
    fgets(line,sizeof(line),f);
    fclose(f);

    char *pos = strchr(line,'\n');
    if(pos){
        *pos = '\0';
    }

    ESP_LOGI(TAG,"Read from file: %s",line);

}