#include "Blinkey.hpp"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <stdio.h>

Blinkey::Blinkey()
{
    
}

Blinkey::Blinkey(gpio_num_t pin){
    this->pin=pin;
    gpio_pad_select_gpio(this->pin);
    gpio_set_direction((gpio_num_t)this->pin,GPIO_MODE_OUTPUT);
    gpio_set_level(this->pin,0);
}

Blinkey::~Blinkey()
{
}


void Blinkey::blink(int times,int freq){
    int setLvl=0;
    int limit=0;
    while (limit<times)
    {
        limit++;
        setLvl= !setLvl;
        gpio_set_level(this->pin,setLvl);
        vTaskDelay((freq/2)/portTICK_PERIOD_MS);
    }
    

}