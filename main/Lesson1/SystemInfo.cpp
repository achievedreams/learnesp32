#include "SystemInfo.hpp"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include <string>
#include <string.h>

SystemInfo::SystemInfo(/* args */)
{
}

SystemInfo::~SystemInfo()
{
}

void SystemInfo::printSys(){
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);

    ESP_LOGW("Chip Cores","%d",chip_info.cores);
    ESP_LOGW("Chip Supports Wifi","%s%s",(chip_info.features&CHIP_FEATURE_BT)? "/BL" :"",
                (chip_info.features&CHIP_FEATURE_BLE)?"/BLE":"");
    ESP_LOGW("Silicon Revision","%d",chip_info.revision);
    ESP_LOGW("Chip Flash Size","%dMB",spi_flash_get_chip_size()/1024/1024);
    ESP_LOGW("Flash is","%s",(chip_info.features&CHIP_FEATURE_EMB_FLASH)?"embedded":"external");

    ESP_LOGW("IDF version","%s",esp_get_idf_version());
    printMacAddress();
}

void SystemInfo::printMacAddress(){

    uint8_t macAddress[6];
    esp_efuse_mac_get_default(macAddress);

    ESP_LOGW("IDF version","%02x:%02x:%02x:%02x:%02x:%02x",macAddress[0],macAddress[1],macAddress[2],macAddress[3],macAddress[4],macAddress[5]);
    
    char macadd[6];
    std::string result;
    for (int i = 0; i < 6; i++)
    {
        if(i==0){
            sprintf(macadd,"%02x",macAddress[i]);
            
        }else{
            
            sprintf(macadd,":%02x",macAddress[i]);
        }
        result+=macadd;
        
    }
}
