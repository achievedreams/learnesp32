#include "RandomNumber.hpp"
#include "esp_system.h"
#include "math.h"
#include "esp_log.h"

#define TAG "DICE"

RandomNumber::RandomNumber(/* args */)
{
}

RandomNumber::~RandomNumber()
{
}

int RandomNumber::diceNumber(){
    int randomNum = esp_random();
    int dice = 1+ (abs(randomNum) % 6 );
    return dice;
}

void RandomNumber::printDice(){
    ESP_LOGI(TAG,"This is roll dice result: %i",diceNumber());
}