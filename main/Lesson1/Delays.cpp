#include "Delays.hpp"
#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define TAG "Delay"

Delay::Delay(){
}
    
void Delay::delayWithPrint(int milisecs){

    int i = 0;

    while(i<10){
        vTaskDelay(milisecs/portTICK_PERIOD_MS);
        ESP_LOGI(TAG, "This is delay for second %i",i++);
    }

}
