#include <stdio.h>
#include "driver/gpio.h"
class Blinkey
{
private:
    gpio_num_t pin;
public:
    Blinkey();
    Blinkey(gpio_num_t pin);
    void blink(int times,int freq);
    ~Blinkey();
};

