
#include <stdio.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Logging.hpp"

Logging::Logging(){
}

Logging::~Logging(){
}


void Logging::printLog(void){
    printf("Hello world\n");
    int number = 0;
    esp_log_level_set("LOG", ESP_LOG_ERROR);
    ESP_LOGE("LOG", "This is an error %d", number++);
    ESP_LOGW("LOG", "This is an warning %d", number++);
    ESP_LOGI("LOG", "This is an info %d", number++);
    ESP_LOGD("LOG", "This is an debug %d", number++);
    ESP_LOGV("LOG", "This is an verbose %d", number++);

    ESP_LOGE("LOG2", "This is an error %d", number++);
    ESP_LOGW("LOG2", "This is an warning %d", number++);
    ESP_LOGI("LOG2", "This is an info %d", number++);
    ESP_LOGD("LOG2", "This is an debug %d", number++);
    ESP_LOGV("LOG2", "This is an verbose %d", number++);
}