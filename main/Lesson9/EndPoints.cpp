#include "EndPoints.hpp"
#include "stdio.h"
#include <string.h>
#include "esp_log.h"
#include "esp_err.h"
#include "esp_http_server.h"
#include "esp_http_client.h"
#include "cJSON.h"
#include "driver/gpio.h"
#include "esp_spiffs.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "connect.hpp"
#include "esp_wifi.h"

extern xSemaphoreHandle wifiInitSemaphore;

#define TAG "SERVER" 
#define TAG2 "FORM"

static esp_err_t baseUrlmethod(httpd_req_t *req){
    ESP_LOGI(TAG,"url accessed is: %s",req->uri);
    esp_vfs_spiffs_conf_t spiffs_config={
        .base_path="/spiffs",
        .partition_label=NULL,
        .max_files=10,
        .format_if_mount_failed=false
    };
    esp_vfs_spiffs_register(&spiffs_config);
    char path[800];
    if(strcmp(req->uri,"/")==0){
        strcpy(path,"/spiffs/index.html");
    }else{
        sprintf(path,"/spiffs%s",req->uri);
    }
    ESP_LOGI("PATH","path requested is: %s",path);
    char *css=strrchr(path,'.');
    if(strcmp(css,".css")==0){
        httpd_resp_set_type(req,"text/css");
    }
    FILE *f=fopen(path,"r");
    if(f==NULL){
        return httpd_resp_send_404(req);
    }else{
        char lineRead[256];
        while(fgets(lineRead,sizeof(lineRead),f)){
            httpd_resp_sendstr_chunk(req,lineRead);
        }
        httpd_resp_sendstr_chunk(req,NULL);
        esp_vfs_spiffs_unregister(NULL);
        return ESP_OK;
    }
    
}

static esp_err_t tempUrlMethod(httpd_req_t *req){
    ESP_LOGI(TAG,"url accessed is: %s",req->uri);
    char *message="{\"temperature\":35.6}";
    return httpd_resp_send(req,message,strlen(message));
}

void resetWifi(void *params){

    vTaskDelay(pdMS_TO_TICKS(1000));
    esp_wifi_stop();
    xSemaphoreGive(wifiInitSemaphore);
    vTaskDelete(NULL);
}

static esp_err_t setwifiUrlMethod(httpd_req_t *req){
    ESP_LOGI(TAG,"url accessed is: %s",req->uri);
    char buff[150];
    memset(&buff,0,sizeof(buff));
    httpd_req_recv(req,buff,req->content_len);
    char *ssid=strtok(buff,"\r\n");
    char *pass=strtok(NULL,"\r\n");
    ssid=strrchr(ssid,'=')+1;
    pass=strrchr(pass,'=')+1;
    ESP_LOGI(TAG2,"ssid is: %s, pwd is : %s",ssid,pass);

    nvs_flash_init();
    nvs_handle_t nvs;
    nvs_open("nvsCreds",NVS_READWRITE,&nvs);
    nvs_set_str(nvs,"ssid",ssid);
    nvs_set_str(nvs,"pass",pass);
    nvs_close(nvs);

    httpd_resp_set_status(req,"303");
    httpd_resp_set_hdr(req,"Location","/wifi-set.html");
    httpd_resp_send(req,NULL,0);

    xTaskCreate(&resetWifi,"wifiReset",2*1024,NULL,6,NULL);

    return ESP_OK;
}


static esp_err_t ledUrlMethod(httpd_req_t *req){
    ESP_LOGI(TAG,"url accessed is: %s",req->uri);
    char buff[150];
    memset(&buff,0,sizeof(buff));
    httpd_req_recv(req,buff,req->content_len);
   
    cJSON *payload = cJSON_Parse(buff);
    cJSON *isLedOn = cJSON_GetObjectItem(payload,"isLedOn");
    gpio_set_level(GPIO_NUM_5,cJSON_IsTrue(isLedOn));
    ESP_LOGI(TAG,"data recieved: %d",cJSON_IsTrue(isLedOn) );
    cJSON_Delete(payload);

    ESP_LOGI(TAG,"data recieved: %s",buff );

    httpd_resp_set_status(req,"204");
    char *message="final here";
    return httpd_resp_send(req,message,sizeof(message));
};

void initLed(){
    gpio_pad_select_gpio(GPIO_NUM_5);
    gpio_set_direction(GPIO_NUM_5,GPIO_MODE_OUTPUT);
    gpio_pulldown_dis(GPIO_NUM_5);
    gpio_pullup_en(GPIO_NUM_5);
}

void registerEndpoints(){
    initLed();
    httpd_handle_t server= NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.uri_match_fn=httpd_uri_match_wildcard;

    
    ESP_LOGI(TAG,"Starting server...\n");
    if(httpd_start(&server,&config) !=ESP_OK){
        ESP_LOGE(TAG,"Server could not start!!\n");
    };
    


    httpd_uri_t temp_endpoint={
        .uri="/api/temperature",
        .method=HTTP_GET,
        .handler=tempUrlMethod   
    };
    httpd_register_uri_handler(server,&temp_endpoint);

    httpd_uri_t led_endpoint={
        .uri="/api/led",
        .method=HTTP_POST,
        .handler=ledUrlMethod
    };
    httpd_register_uri_handler(server,&led_endpoint);

    httpd_uri_t setwifi_endpoint={
        .uri="/api/setwifi",
        .method=HTTP_POST,
        .handler=setwifiUrlMethod
    };
    httpd_register_uri_handler(server,&setwifi_endpoint);

    httpd_uri_t base_endpoint={
        .uri="/*",
        .method=HTTP_GET,
        .handler=baseUrlmethod
    };
    httpd_register_uri_handler(server,&base_endpoint);


}