#include "connect.hpp"
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_netif.h"

//1086321
#define TAG4 "CONNECTION"
#define TAG5 "WIFI_INIT"
#define TAG6 "ON STA INIT"
#define TAG7 "ON AP INIT"

extern xSemaphoreHandle connectionSemaphore;
extern xSemaphoreHandle wifiInitSemaphore;


static void my_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    switch (event_id)
    {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG4, "connecting...");
        esp_wifi_connect();
        break;

    case SYSTEM_EVENT_STA_CONNECTED:
        ESP_LOGI(TAG4, "connected...");
        break;

    case IP_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG4, "got ip...");

        break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG4, "disconnected...");
        break;

    default:
        break;
    }
}

void connectSTA(char *ssid, char *pass)
{
    ESP_LOGW(TAG6, "inside STA");
    if (ssid != NULL && pass != NULL)
    {
        ESP_LOGW(TAG6,"CRED ssid: %s, pwd: %s", ssid, pass);
        esp_netif_t *netifConf = NULL;
        netifConf = esp_netif_get_handle_from_ifkey("WIFI_AP_DEF");
        if (netifConf == NULL)
        {
            ESP_LOGW(TAG6, "netif is not AP");
            netifConf = esp_netif_get_handle_from_ifkey("WIFI_STA_DEF");
            if(netifConf==NULL){
                ESP_LOGW(TAG6, "netif is not STA also");
            }else{
                ESP_LOGW(TAG6, "netif is STA");
            }
        }
        else
        {   
            ESP_LOGW(TAG6, "netif is AP and we reset");
            esp_netif_destroy_default_wifi(netifConf);
            esp_netif_create_default_wifi_sta();
            commonWifiInit();
        }

        wifi_config_t wifi_configSta;
        memset(&wifi_configSta, 0, sizeof(wifi_configSta));
        strcpy((char *)wifi_configSta.sta.ssid, ssid);
        strcpy((char *)wifi_configSta.sta.password, pass);
        ESP_LOGW(TAG6, "in config ssid: %s, pass: %s", wifi_configSta.sta.ssid, wifi_configSta.sta.password);
        esp_wifi_set_mode(WIFI_MODE_STA);
        esp_wifi_set_config(WIFI_IF_STA, &wifi_configSta);
    }
    else
    {
        ESP_LOGW(TAG6, "STA connection unconfigured");
    }
}

void connectAP()
{
    ESP_LOGW(TAG7, "inside AP");

    esp_netif_create_default_wifi_ap();
    commonWifiInit();
 
    wifi_ap_config_t ap_config={
        "EspSSID3",
        "Polycom!",
        sizeof("EspSSID3"),
        11,
        WIFI_AUTH_WPA_WPA2_PSK,
        0,
        10,
        100,
        WIFI_CIPHER_TYPE_TKIP,
        false
    };
    
    

    wifi_config_t wifi_config={
        .ap=ap_config
    };

    esp_wifi_set_mode(WIFI_MODE_AP);
    esp_wifi_set_config(WIFI_IF_AP, &wifi_config);

}

void commonWifiInit(){
        wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));
        ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, my_event_handler, NULL));
        ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, my_event_handler, NULL));
        ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
}

void wifiInit2(void *params)
{

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    
    while (true)
    {
        if (xSemaphoreTake(wifiInitSemaphore, portMAX_DELAY))
        {

            nvs_handle_t nvs;
            nvs_open("nvsCreds", NVS_READWRITE, &nvs);

            size_t ssidLen, passLen;
            char *ssid = NULL, *pass = NULL;

            if (nvs_get_str(nvs, "ssid", NULL, &ssidLen) == ESP_OK)
            {
                if (ssidLen > 0)
                {
                    ssid = (char *)malloc(ssidLen);
                    nvs_get_str(nvs, "ssid", ssid, &ssidLen);
                }
                if (nvs_get_str(nvs, "pass", NULL, &passLen) == ESP_OK)
                {
                    if (passLen > 0)
                    {
                        pass = (char *)malloc(passLen);
                        nvs_get_str(nvs, "pass", pass, &passLen);
                    }
                }
            }

            if (ssid != NULL && pass != NULL)
            {
                connectSTA(ssid, pass);
            }
            else
            {
                connectAP();
            }

            
            
            
            
            ESP_LOGW(TAG5, "After reInit of mode");
            wifi_mode_t *wifiMode=NULL;
            esp_wifi_get_mode(wifiMode);
            if(wifiMode!=NULL){
                ESP_LOGW(TAG5, "key: %s", (char *)wifiMode);
            }else{
                ESP_LOGW(TAG5, "wifi mode is null");
            }
            esp_netif_t *apNetIf = NULL;
            while ((apNetIf = esp_netif_next(apNetIf)) != NULL)
            {
                ESP_LOGW(TAG5, "desc: %s", esp_netif_get_desc(apNetIf));
                ESP_LOGW(TAG5, "key: %s", esp_netif_get_ifkey(apNetIf));
                
            }
            
            ESP_ERROR_CHECK(esp_wifi_start());
            xSemaphoreGive(connectionSemaphore);
            if (ssid != NULL)
                free(ssid);
            if (pass != NULL)
                free(pass);
        }
    }
}