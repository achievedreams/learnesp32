#include "Server.hpp"
#include "stdlib.h"
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "connect.hpp"
#include "esp_wifi.h"
#include "EndPoints.hpp"

#define TAG "DATA" 
#define TAG4 "CONNECTION" 

xSemaphoreHandle connectionSemaphore;
xSemaphoreHandle wifiInitSemaphore;

void onConnect(void *param){

    while (true)
    {
        if(xSemaphoreTake(connectionSemaphore,10*1000*portTICK_PERIOD_MS)){
            ESP_LOGI(TAG4,"Processing...");
            registerEndpoints();
     
            ESP_LOGI(TAG4,"Registered Endpoints...");
            // ESP_LOGI(TAG4,"Finished Processing...");
            // esp_wifi_disconnect();
            xSemaphoreTake(connectionSemaphore,portMAX_DELAY);
        }else{
            ESP_LOGE(TAG4,"Can not connect...");
            for (int i = 5; i >=0; i--)
            {
                ESP_LOGE(TAG4,"...%d",i);
                vTaskDelay(pdMS_TO_TICKS(1000));
            }
            esp_restart();
        }
    }   
}



void runServer(){
  
    connectionSemaphore = xSemaphoreCreateBinary();
    wifiInitSemaphore = xSemaphoreCreateBinary();
 
    xTaskCreate(&wifiInit2,"on WifiInit",1024*5,NULL,3,NULL);
    xSemaphoreGive(wifiInitSemaphore);
    xTaskCreate(&onConnect,"on Connect",1024*6,NULL,5,NULL);

}