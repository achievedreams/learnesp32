#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Lesson1/Delays.hpp"
#include "Lesson1/Logging.hpp"
#include "Lesson1/RandomNumber.hpp"
#include "Lesson1/Blinkey.hpp"
#include "Lesson1/KeyboardInput.hpp"
#include "Lesson1/SystemInfo.hpp"
#include "Lesson2/MyTasks.hpp"
#include "Lesson3/storage.hpp"
#include "Lesson4/interrupt.hpp"
#include "Lesson4/dacadc.hpp"
#include "driver/dac.h"
#include "driver/gpio.h"
#include "Lesson4/myadc.hpp"
#include "Lesson4/myhallsensor.hpp"
#include "Lesson4/myledc.hpp"
#include "Lesson4/mytouch.hpp"
#include "Lesson5/myUart.hpp"
#include "Lesson5/BME680.hpp"
#include "Lesson5/Sdcard.hpp"
#include "Lesson6/myDebug.hpp"
#include "driver/i2c.h"
#include "Lesson7/lightSleep.hpp"
#include "Lesson7/deepSleep.hpp"
#include "Lesson8/MyWifi.hpp"
// #include "Lesson9/connect.hpp"
#include "Lesson9/Server.hpp"
// #include "Lesson9/EndPoints.hpp"

extern "C"
{
    void app_main();
}

void bmeRun(){
    vTaskDelay(pdMS_TO_TICKS(1000));
//    runUart();
    BME680 bme;
    
    uint8_t sda=21;//to be fixed
    uint8_t clk=22;//to be fixed
    bool sdo=false;//0x76 ADDRESS
    bool csb=true;//i2c enable
    bme.init(sda,clk,sdo,csb);
    for(int i=0;i<=10;i++){
        
        
        bme.run(bme.OverSamplingTPH::OVERSAMPLING_X8,bme.OverSamplingTPH::OVERSAMPLING_X8,
                bme.OverSamplingTPH::OVERSAMPLING_X8,bme.IIRFilterCoef::IIR_FILTER_COEF_7,
                bme.RegNames::GAS_RES_HEAT_REG_0,300,bme.RegNames::GAS_WAIT_REG_0,400);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void lesson1(){
    /*
    Delay myDelay;
    myDelay.delayWithPrint(1000);

    Logging mylog;
    mylog.printLog();

    RandomNumber rand;
    rand.printDice();
*/
    Blinkey bl = Blinkey((gpio_num_t)23);
    bl.blink(16, 1000);
    /*
    KeyboardInput kI;
    kI.inputStr();
    */
    SystemInfo sysInfo;
    sysInfo.printSys();
}

void lesson2(){
    
    // runTasks();
    // runMutex();
    // runBinSemaph();
    // runQueue();
    // runEventGroups();
    // runTimer();
    // runPrecisionTimer();
}

void lesson3(){
    // printFile();
    // printImageSize();
    // nvsBasic();
    // nvsCustomPartition();
    // nvsCustomPartitionStruct();
    spiffs();
}

void lesson4(){

    // runInterupt(GPIO_NUM_36);
    // runDac(DAC_CHANNEL_1);
    // runAdc();
    // runLedC();
    // runHall();
    runTouch();
}

void lesson5(){
   
//    runUart();
//    bmeRun();
    runSd();
 

}

void lesson6(){
    runDeb();
}

void lesson7(){

    // runLightSleep();
    // runDeepSleepTimer();
    // runDeepSleepEx0();
    runDeepSleepEx1();
    // runHibernate();
}

void lesson8(){
    // runConnect();
    // wifiScanner();
    // ntpTime();
    // wifiConnectWithHandler();
}

void lesson9(){
    runServer();
}

void app_main()
{
    lesson9();
}
