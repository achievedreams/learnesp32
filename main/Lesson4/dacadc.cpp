#include "dacadc.hpp"
#include "driver/dac.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"

void runDac(dac_channel_t channel)
{

    dac_output_enable(channel);
    while (true)
    {
       
        ESP_LOGI("DAC","going up");
        vTaskDelay(500/portTICK_PERIOD_MS);
        for ( int i = 0; i <= 255; i++)
        {
            dac_output_voltage(channel, i);
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }

        ESP_LOGI("DAC","going down");
        vTaskDelay(500/portTICK_PERIOD_MS);
        for (int i = 255; i >= 0; i--)
        {
            dac_output_voltage(channel, i);
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
    }
}