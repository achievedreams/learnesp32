#include "mytouch.hpp"
#include "driver/touch_pad.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "soc/touch_sensor_channel.h"

void runTouch(){
    touch_pad_init();
    touch_pad_set_voltage(TOUCH_HVOLT_2V7,TOUCH_LVOLT_0V5,TOUCH_HVOLT_ATTEN_1V);
    touch_pad_config(TOUCH_PAD_GPIO13_CHANNEL,-1);

    touch_pad_filter_start(10);
    uint16_t val,raw,filt;

    while (true)
    {
        touch_pad_read(TOUCH_PAD_GPIO13_CHANNEL,&val);
        touch_pad_read_filtered(TOUCH_PAD_GPIO13_CHANNEL,&filt);
        touch_pad_read_raw_data(TOUCH_PAD_GPIO13_CHANNEL,&raw);
        printf("val: %d, raw: %d, filtered: %d\n",val,raw,filt);
        vTaskDelay(500/portTICK_PERIOD_MS);

    }
    
}