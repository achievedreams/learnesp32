#include "interrupt.hpp"
#include "stdio.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"


xQueueHandle queuet;

static void IRAM_ATTR gpio_isr_handler(void *args){

    int pinNumber=(int)args;
    xQueueSendFromISR(queuet,&pinNumber,NULL);
}

void buttonPushedTask(void *args){

    int pinNumber,count=0;
    while (true)
    {
        if(xQueueReceive(queuet,&pinNumber,portMAX_DELAY)){

            gpio_isr_handler_remove((gpio_num_t)pinNumber);

            do
            {
                vTaskDelay(20/portTICK_PERIOD_MS);
            } while (gpio_get_level((gpio_num_t)pinNumber)==1);
            

            printf("GPIO %d was pressed %d times. The state is %d\n",pinNumber,count++,gpio_get_level(GPIO_NUM_36));
        
            gpio_isr_handler_add((gpio_num_t)pinNumber,gpio_isr_handler,(void *)pinNumber);
        }
    }
    
}


void runInterupt(gpio_num_t pin){

    gpio_pad_select_gpio(pin);
    gpio_set_direction(pin,GPIO_MODE_INPUT);
    gpio_pulldown_en(pin);
    gpio_pullup_dis(pin);
    gpio_set_intr_type(pin,GPIO_INTR_POSEDGE);


    queuet = xQueueCreate(10,sizeof(int));
    xTaskCreate(buttonPushedTask,"buttonPushedTask",2048,NULL,1,NULL);

    gpio_install_isr_service(0);
    gpio_isr_handler_add(pin,gpio_isr_handler,(void *)pin);


}

