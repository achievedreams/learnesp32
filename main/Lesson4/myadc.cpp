#include "myadc.hpp"
#include "driver/adc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

void runAdc()
{
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_0,ADC_ATTEN_6db);//3.3 v input

    while (true)
    {
        int val=adc1_get_raw(ADC1_CHANNEL_0);
        printf("vai is %d",val);
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
    
}