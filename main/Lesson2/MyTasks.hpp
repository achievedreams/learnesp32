#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"

void sender( void * params);
void receiver( void * params);
void runTasks();

void task1(void *params);
void task2(void *params);
void writeToBus(char *params);
void runMutex();

void task3(void *params);
void listenForHttp(void * params);
void runBinSemaph();

void task4(void *params);
void listenForHttp2(void * params);
void runQueue();

void timer_callback(void * args);
void runPrecisionTimer();


void listenForHttp3(void * params);
void listenForBLE(void * params);
void task5(void * params);
void runEventGroups();

void on_timer(xTimerHandle xTimer);
void runTimer();

void timer_callback(void * args);
void runPrecisionTimer();
