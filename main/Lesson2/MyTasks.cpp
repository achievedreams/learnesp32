#include "MyTasks.hpp"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"
#include "freertos/timers.h"
#include "esp_system.h"
#include "driver/gpio.h"


static TaskHandle_t receiverHandler = NULL;
xSemaphoreHandle mutexBus;
xSemaphoreHandle binSema;
xQueueHandle queue;
EventGroupHandle_t eventGrp;
const int httpBit=BIT0;
const int bleBit=BIT1;
xTimerHandle xTimer;
esp_timer_handle_t esp_timer_handler;
esp_timer_dispatch_t esp_timer_dispatch;
bool on;

esp_timer_create_args_t esp_timer_args={
    .callback = timer_callback,
    .arg=&on,
    .dispatch_method = esp_timer_dispatch,  //!< Call the callback from task or from ISR
    .name = "myTimer",           //!< Timer name, used in esp_timer_dump function
    .skip_unhandled_events =true
    
};

void sender(void *params)
{

    while (true)
    {
        xTaskNotify(receiverHandler, (1 << 0), eSetValueWithOverwrite);
        vTaskDelay(400 / portTICK_PERIOD_MS);
        xTaskNotify(receiverHandler, (1 << 2), eSetValueWithOverwrite);
        vTaskDelay(400 / portTICK_PERIOD_MS);
        xTaskNotify(receiverHandler, (1 << 3), eSetValueWithOverwrite);
        vTaskDelay(400 / portTICK_PERIOD_MS);
    }
}

void receiver(void *params)
{
    uint state;
    while (true)
    {
        xTaskNotifyWait(0, 0, &state, portMAX_DELAY);
        printf("The state is %d\n", state);
    }
}

void task1(void *params)
{
    while (true)
    {
        printf("Reading Temperature\n");
        if (xSemaphoreTake(mutexBus, 1000 / portTICK_PERIOD_MS))
        {
            char mes[50] = {"Temperature is 22 C\n"};
            writeToBus(mes);
            xSemaphoreGive(mutexBus);
        }
        else
        {
            printf("Writing Temperature timed out\n");
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void task2(void *params)
{
    while (true)
    {
        printf("Reading Humidity\n");
        if (xSemaphoreTake(mutexBus, 1000 / portTICK_PERIOD_MS))
        {
            char mes[20] = {"Humidity is 55%\n"};
            writeToBus(mes);
            xSemaphoreGive(mutexBus);
        }
        else
        {
            printf("Writing Humidity timed out\n");
        }
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

void writeToBus(char *message)
{
    printf(message);
}

void listenForHttp(void *params)
{
    while (true)
    {
        printf("receive message\n");
        xSemaphoreGive(binSema);
        printf("doing something else\n");
        vTaskDelay(2000/ portTICK_PERIOD_MS);
    }
}

void task3(void * params)
{
    while (true)
    {
        xSemaphoreTake(binSema, portMAX_DELAY);
        printf("process\n");
    }
}

void listenForHttp2(void *params)
{
    int count=0;
    while (true)
    {
        count++;
        printf("receive message\n");
        long flag=xQueueSend(queue,&count,1000/portTICK_PERIOD_MS);
        if(flag){
            printf("added to the queue\n");
        }else{
            printf("faild to add to the queue\n");
        }
        vTaskDelay(2000/ portTICK_PERIOD_MS);
    }
}

void task4(void * params)
{
    while (true)
    {   
        int rec;
        if(xQueueReceive(queue,&rec,2000/portTICK_PERIOD_MS)){

            printf("process %d\n",rec);
        }else{
            printf("failed to process\n");
        }
    }
}

void listenForHttp3(void * params){
    while (true)
    {
        xEventGroupSetBits(eventGrp,httpBit);
        printf("got http info\n");
        vTaskDelay(2000/portTICK_PERIOD_MS);
    }
    
}

void listenForBLE(void * params){
    while (true)
    {

        xEventGroupSetBits(eventGrp,bleBit);
        printf("got BLE info\n");
        vTaskDelay(5000/portTICK_PERIOD_MS);
    }
    
}

void task5(void * params){
    while (true)
    {
        xEventGroupWaitBits(eventGrp,httpBit | bleBit,true,true,portMAX_DELAY);
        printf("got http and ble info\n");

    }
    
}

void on_timer(xTimerHandle xTimer){
    printf("App on_timer at %lld\n",esp_timer_get_time()/1000);
}

void timer_callback(void * args){    
    gpio_set_level(GPIO_NUM_4,!*(bool *)args);
    on=!on;
}

void runTasks()
{
    xTaskCreate(&receiver, "sender", 2048, NULL, 2, &receiverHandler);
    xTaskCreate(&sender, "receiver", 2048, NULL, 2, NULL);
}

void runMutex()
{
    mutexBus = xSemaphoreCreateMutex();
    xTaskCreate(&task1, "temp", 2048, NULL, 2, NULL);
    xTaskCreate(&task2, "hum", 2048, NULL, 2, NULL);
}

void runBinSemaph()
{
    binSema = xSemaphoreCreateBinary();
   
    xTaskCreate(&task3, "task3", 2048, NULL, 1, NULL);
    xTaskCreate(&listenForHttp, "http", 2048, NULL, 3, NULL);
}

void runQueue(){

    queue = xQueueCreate(5,sizeof(int));
    xTaskCreate(&listenForHttp2, "http2", 2048, NULL, 3, NULL);
    xTaskCreate(&task4, "task4", 2048, NULL, 1, NULL);
}

void runEventGroups(){

    eventGrp = xEventGroupCreate();
    xTaskCreate(&task5,"task5",2048,NULL,2,NULL);
    xTaskCreate(&listenForHttp3,"listenForHttp3",2048,NULL,2,NULL);
    xTaskCreate(&listenForBLE,"listenForBLE",2048,NULL,2,NULL);

}

void runTimer(){

    printf("App at %lld\n",esp_timer_get_time()/1000);
    xTimer = xTimerCreate("myTimer",pdMS_TO_TICKS(1000),true,NULL,on_timer);
    xTimerStart(xTimer,0);
}

void runPrecisionTimer(){

    gpio_pad_select_gpio(GPIO_NUM_4);
    gpio_set_direction(GPIO_NUM_4,GPIO_MODE_OUTPUT);

    esp_timer_create(&esp_timer_args,&esp_timer_handler);
    esp_timer_start_periodic(esp_timer_handler,1000000);

    int x=0;

    for(x=0;x<1000;x++){
        esp_timer_dump(stdout);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
    esp_timer_stop(esp_timer_handler);
    esp_timer_delete(esp_timer_handler);
}