#include "storage.hpp"
#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_system.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_spiffs.h"
#include <sys/unistd.h>
#include <sys/stat.h>
#include "stdlib.h"
#include "dirent.h"


#define TAG "NVS"
#define TAG2 "SPIFFS"

nvs_handle_t handle;

typedef struct cat_struct
{
    int age;
    char name[10];
    int id;
} Cat;

void printFile()
{

    extern const char sampleFile[] asm("_binary_sample_esp32_txt_start");
    printf("sample = %s\n", sampleFile);
}

void printImageSize()
{

    extern const char imgStart[] asm("_binary_ESP32_Pinout_png_start");
    extern const char imgEnd[] asm("_binary_ESP32_Pinout_png_end");
    const unsigned int pngSize = imgEnd - imgStart;
    printf("sample = %d\n", pngSize);
}

void nvsBasic()
{
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(nvs_open("store", NVS_READWRITE, &handle));

    int32_t val = 0;

    // ESP_ERROR_CHECK(nvs_get_i32(handle,"theVal",&val));

    esp_err_t res = nvs_get_i32(handle, "theVal", &val);

    switch (res)
    {
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(TAG, "Value not set yet");
        break;
    case ESP_OK:
        ESP_LOGI(TAG, "Value is %d", val);
        break;
    default:
        break;
        ESP_LOGI(TAG, "This is default value %d", val);
    }

    val++;
    ESP_ERROR_CHECK(nvs_set_i32(handle, "theVal", val));
    ESP_ERROR_CHECK(nvs_commit(handle));
    nvs_close(handle);
}

void nvsCustomPartition()
{
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    ESP_ERROR_CHECK(nvs_flash_init_partition("MyNvs"));

    ESP_ERROR_CHECK(nvs_open_from_partition("MyNvs", "store", NVS_READWRITE, &handle));

    nvs_stats_t stats;
    nvs_get_stats("MyNvs", &stats);

    ESP_LOGI(TAG, "used: %d, free: %d, total: %d, namespace count: %d",
             stats.used_entries, stats.free_entries, stats.total_entries, stats.namespace_count);

    int32_t val = 0;

    int32_t val2 = 10;
    // ESP_ERROR_CHECK(nvs_get_i32(handle,"theVal",&val));

    esp_err_t res = nvs_get_i32(handle, "theVal", &val);
    esp_err_t res2 = nvs_get_i32(handle, "theVal2", &val2);

    switch (res)
    {
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(TAG, "Value not set yet");
        break;
    case ESP_OK:
        ESP_LOGI(TAG, "Value is %d", val);
        break;
    default:
        break;
        ESP_LOGI(TAG, "This is default value %d", val);
    }

    switch (res2)
    {
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(TAG, "Value2 not set yet");
        break;
    case ESP_OK:
        ESP_LOGI(TAG, "Value is %d", val2);
        break;
    default:
        break;
        ESP_LOGI(TAG, "This is default value2 %d", val);
    }

    val++;
    val2++;
    ESP_ERROR_CHECK(nvs_set_i32(handle, "theVal", val));
    ESP_ERROR_CHECK(nvs_set_i32(handle, "theVal2", val2));
    ESP_ERROR_CHECK(nvs_commit(handle));
    nvs_close(handle);
}

void nvsCustomPartitionStruct()
{
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    ESP_ERROR_CHECK(nvs_flash_init_partition("MyNvs"));

    ESP_ERROR_CHECK(nvs_open_from_partition("MyNvs", "cat_store", NVS_READWRITE, &handle));

    nvs_stats_t stats;
    nvs_get_stats("MyNvs", &stats);

    ESP_LOGI(TAG, "used: %d, free: %d, total: %d, namespace count: %d",
             stats.used_entries, stats.free_entries, stats.total_entries, stats.namespace_count);

    char catkey[15];
    Cat myCat;
    // ESP_ERROR_CHECK(nvs_get_i32(handle,"theVal",&val));
    size_t catSize = sizeof(myCat);
    int i = 3;
    sprintf(catkey, "cat%d", i);
    esp_err_t res = nvs_get_blob(handle, catkey, (void *)&myCat, &catSize);

    switch (res)
    {
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGE(TAG, "Value not set yet");
        break;
    case ESP_OK:
        ESP_LOGI(TAG, "Value : catName: %s, catAge: %d, catKey: %d", myCat.name, myCat.age, myCat.id);
        break;
    default:
        break;
        ESP_LOGI(TAG, "This is default values");
    }

    sprintf(catkey, "cat%d", i);
    sprintf(myCat.name, "TheCat");
    myCat.age = i + 20;
    myCat.id = i;
    ESP_ERROR_CHECK(nvs_set_blob(handle, catkey, (void *)&myCat, sizeof(myCat)));
    ESP_ERROR_CHECK(nvs_commit(handle));
    nvs_close(handle);
    i++;
}

void spiffs()
{

    printf("loop start\n");
    esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = "storage",
      .max_files = 5,
      .format_if_mount_failed = true
    };


    esp_vfs_spiffs_register(&conf);
    printf(esp_spiffs_mounted("storage")==true?"yes\n":"no\n");
    

    DIR *dir= opendir("/spiffs");
    struct dirent *entry;

    int i=0;
    printf("loop : %d\n",i++);
    while ((entry=readdir(dir))!=NULL){
        printf("loop : %d\n",i++);
        char fullPath[300];
        sprintf(fullPath,"/spiffs/%s",entry->d_name);
        struct stat entryStat;
        if(stat(fullPath,&entryStat)== -1){
            ESP_LOGE(TAG2, "error getting stats for %s",fullPath);
        }else{
            ESP_LOGI(TAG,"full path = %s, file size = %ld", fullPath,entryStat.st_size);
        }

    }
    printf("loop end\n");


    size_t total=0, used = 0;
    esp_spiffs_info("storage",&total,&used);
    ESP_LOGI(TAG2,"total: %d, used: %d",total,used);



    // FILE *f = fopen("/spiffs/sub/data.txt", "a");
    // if (f == NULL) {
    //     ESP_LOGE(TAG, "Failed to open file for writing");
    //     return;
    // }
    // fprintf(f, "Hello World!\n");
    // fclose(f);
    // ESP_LOGI(TAG, "File written");


    char line[256];
    FILE *file = fopen("/spiffs/sub/data.txt","r");


    if (file == NULL)
    {
        ESP_LOGE(TAG2, "File not found");
    }
    else
    {
        while (fgets(line, sizeof(line),file) != NULL)
        {
            printf("%s", line);
        }
        fclose(file);
    }

    esp_vfs_spiffs_unregister(conf.partition_label);
}
