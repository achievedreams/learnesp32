#include "lightSleep.hpp"
#include "stdio.h"
#include "stdlib.h"
#include "esp_sleep.h"
#include "esp_log.h"
#include "time.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sys/time.h"
#include "esp32/rom/uart.h"
#include "driver/rtc_io.h"




void runLightSleep(){

    gpio_num_t pin=GPIO_NUM_0;
    gpio_pad_select_gpio(pin);
    gpio_set_direction(pin,GPIO_MODE_INPUT);
    gpio_wakeup_enable(pin,GPIO_INTR_LOW_LEVEL);

    esp_sleep_enable_gpio_wakeup();
    esp_sleep_enable_timer_wakeup(5000000);

    while(true){

        if(rtc_gpio_get_level(pin) == 0){
            printf("realease button\n");
            do
            {
                vTaskDelay(pdMS_TO_TICKS(10));
            } while( rtc_gpio_get_level(pin) == 0 );
        }

        printf("going for nap\n");

        uart_tx_wait_idle(CONFIG_ESP_CONSOLE_UART_NUM);
        int64_t before = esp_timer_get_time();
        esp_light_sleep_start();
        int64_t after = esp_timer_get_time();
    
        esp_sleep_wakeup_cause_t reason = esp_sleep_get_wakeup_cause();

        printf("napped for %lld\n", (after-before)/1000);
        printf("wake up reason %s\n",reason == ESP_SLEEP_WAKEUP_TIMER? "timer":"button");
    }

}