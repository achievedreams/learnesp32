#include "deepSleep.hpp"
#include "stdio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_sleep.h"
#include "driver/rtc_io.h"


RTC_DATA_ATTR  int counter=0;

void runDeepSleepTimer(){

    esp_sleep_enable_timer_wakeup(3*1000000);
    printf("going to sleep at: %d\n",counter++);
    esp_deep_sleep_start();
    printf("i am awake");
}

void runDeepSleepEx0(){

    gpio_num_t pin16=GPIO_NUM_25;
    rtc_gpio_pullup_en(pin16);
    rtc_gpio_pulldown_dis(pin16);
    esp_sleep_enable_ext0_wakeup(pin16,0);


    printf("going to sleep at: %d\n",counter++);
    esp_deep_sleep_start();
    printf("i am awake");
}

void runDeepSleepEx1(){

    gpio_num_t pin25=GPIO_NUM_25;
    gpio_num_t pin26=GPIO_NUM_26;
    rtc_gpio_pullup_dis(pin25);
    rtc_gpio_pullup_dis(pin26);
    rtc_gpio_pulldown_en(pin25);
    rtc_gpio_pulldown_en(pin26);

    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH,ESP_PD_OPTION_ON);
    uint64_t mask = (1ULL << pin25) | (1ULL << pin26);

    esp_sleep_enable_ext1_wakeup(mask,ESP_EXT1_WAKEUP_ANY_HIGH);


    printf("going to sleep at: %d\n",counter++);
    esp_deep_sleep_start();
    printf("i am awake");
}

void runHibernate(){
    
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH,ESP_PD_OPTION_OFF);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM,ESP_PD_OPTION_OFF);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_SLOW_MEM,ESP_PD_OPTION_OFF);

    esp_sleep_enable_timer_wakeup(3*1000000);
    // printf("going to sleep at: %d\n",counter++);
    
    esp_deep_sleep_start();
    printf("i am awake");
}