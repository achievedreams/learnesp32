#include "MyWifi.hpp"
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_http_client.h"
#include "protocol_examples_common.h"
#include "time.h"
#include "esp_sntp.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "cJSON.h"


#define MAX_APs 20

#define TAG3 "NTP"
#define TAG4 "CONNECTION" 
#define TAG5 "CLIENT" 
#define TAG6 "JSON PARSER"
#define TAG7 "DATA_GET"
#define TAG8 "DATA_POST"
#define SSID CONFIG_WIFI_SSID
#define PASSWORD CONFIG_WIFI_PASSWORD

xSemaphoreHandle onConnectionHandler;

char * token="Bearer 04909151e4f8fad09a1d4db0d6125baebd9f7cf7c3f91ca5010d434e3fcc8c9c";

char* buffer;
int bufIndex=0;

typedef struct{
    char* key;
    char* val;
} Headers;

typedef enum{
    GET,
    POST,
    DELETE
}http_methods;

struct FetchParams {
    void (*onGotData)(char* incomingData, char* outputData,http_methods method);
    char message[800];
    int headerCount;
    Headers header[5];
    http_methods method;
    char* body;
    int status;
};

esp_err_t clientEvent(esp_http_client_event_t *evt){

    struct FetchParams *fparam=(struct FetchParams *)evt->user_data;
    switch (evt->event_id)
    {
    case HTTP_EVENT_ON_DATA:
        ESP_LOGI(TAG5,"HTTP_EVENT_ON_DATA Len: %d",evt->data_len);
        printf("%.*s\n.",evt->data_len,(char*)evt->data);
        if (buffer==NULL){
            buffer=(char *)malloc(evt->data_len);
        }else{
            buffer=(char *)realloc(buffer,evt->data_len+bufIndex);
        }
        memcpy(&buffer[bufIndex],evt->data,evt->data_len);
        bufIndex+=evt->data_len;
        break;
    
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGI(TAG5,"HTTP_EVENT_ON_FINISH");
        buffer=(char *)realloc(buffer,bufIndex+1);
        memcpy(&buffer[bufIndex],"\0",1);
        //onGotData(buffer);
        if(fparam->method==GET){
            fparam->onGotData(buffer,fparam->message,GET);
        }else{
            fparam->onGotData(buffer,fparam->message,POST);
        }
        
        // ESP_LOGI(TAG5,"%s",fparam->message);
        buffer=NULL;
        bufIndex=0;

    default:
        break;
    }
    return ESP_OK;
}

void onGotData(char* incomingData, char* outputData,http_methods method){

    cJSON *payload=cJSON_Parse(incomingData);
    if(method==GET){
        printf("onGotDataGet\n");
        
        cJSON *contents=cJSON_GetObjectItem(payload,"contents");
        if(contents!=NULL){
            cJSON *quotes=cJSON_GetObjectItem(contents,"quotes");
            cJSON *copyright=cJSON_GetObjectItem(payload,"copyright");
            cJSON *yearItem=cJSON_GetObjectItem(copyright,"year");
            // ESP_LOGI(TAG6,"%d",(int)cJSON_GetNumberValue(yearItem));
            // ESP_LOGI(TAG6,"%d",yearItem->valueint);
            cJSON *quoteItem;
            cJSON_ArrayForEach(quoteItem,quotes){
                cJSON *quote=cJSON_GetObjectItem(quoteItem,"quote");
                // ESP_LOGI(TAG6,"%s",quote->valuestring);
                strcpy(outputData,quote->valuestring);
            }
        }else{
            cJSON *error=cJSON_GetObjectItem(payload,"error");
            cJSON *message=cJSON_GetObjectItem(error,"message");
            strcpy(outputData,message->valuestring);
        }
    }else{
        printf("onGotDataPost\n");
        
        cJSON *data=cJSON_GetObjectItem(payload,"data");
        cJSON *dataItem;
        cJSON_ArrayForEach(dataItem,data){
            cJSON *message=cJSON_GetObjectItem(dataItem,"message");
                // ESP_LOGI(TAG6,"%s",quote->valuestring);
                strcpy(outputData,message->valuestring);
        }
    }
    cJSON_Delete(payload);

}

static char* getAuthenticationModeName(wifi_auth_mode_t auth_mode){
    char*  name[]={"OPEN","WEP","WPA PSK","WPA2 PSK","WPA WPA2 PSK","MAX"};
    return name[auth_mode];
}

static void event_handler(void* event_handler_arg,esp_event_base_t event_base,int32_t event_id,void* event_data){
    switch (event_id)
    {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG4,"connecting...");
        esp_wifi_connect();
        break;

        case SYSTEM_EVENT_STA_CONNECTED:
        ESP_LOGI(TAG4,"connected...");
        break;  

        case IP_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG4,"got ip...");
         xSemaphoreGive(onConnectionHandler);
        break;

        case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG4,"disconnected...");
        break;

    default:
        break;
    }
}

void wifiInit(){
    
    ESP_ERROR_CHECK(nvs_flash_init());
    // tcpip_adapter_init();
    // ESP_ERROR_CHECK(esp_event_loop_init(event_handler,NULL));
    ESP_ERROR_CHECK(esp_netif_init());
    esp_event_loop_create_default();
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t wifi_init_config= WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));
    // ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,ESP_EVENT_ANY_ID,event_handler,NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT,IP_EVENT_STA_GOT_IP,event_handler,NULL));

    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

    wifi_config_t wifi_config={
        .sta ={
            SSID,
            PASSWORD
        }
    };
    
    esp_wifi_set_config(WIFI_IF_STA,&wifi_config);
    ESP_ERROR_CHECK(esp_wifi_start());
}

void print_time(long time, const char* message){

    setenv("TZ","EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:0",1);
    tzset();
    struct tm *timeinfo = localtime(&time);

    char buffer[50];
    strftime(buffer,sizeof(buffer),"%c",timeinfo);
    ESP_LOGI(TAG3,"message: %s: %s",message,buffer);
}

void on_got_time(struct timeval *tv){
    
    printf("Secs: %ld",tv->tv_sec);
    print_time(tv->tv_sec,"time at callback");

    for(int i=0; i<5;i++){
        time_t now=0;
        time(&now);
        print_time(now,"Time at the beginning:");
        vTaskDelay(pdMS_TO_TICKS(1000));
    }

    esp_restart();
}

void fetch(char* url,struct FetchParams *fparams){

    esp_http_client_config_t client_config={
        .url=url,
        .event_handler=clientEvent,
        .user_data = fparams
    };

    esp_http_client_handle_t client=esp_http_client_init(&client_config);
    if(fparams->method==POST){
        esp_http_client_set_method(client,HTTP_METHOD_POST);
    }
    for(int i =0;i<fparams->headerCount;i++){
        esp_http_client_set_header(client,fparams->header[i].key,fparams->header[i].val);
    }
    if(fparams->body!=NULL){
        esp_http_client_set_post_field(client,fparams->body,strlen(fparams->body));
    }
    
    
    esp_err_t err = esp_http_client_perform(client);
    fparams->status=esp_http_client_get_status_code(client);
    if(err==ESP_OK){
        ESP_LOGI(TAG5,"HTTP GET METHOD status= %d, content-length=%d",
            esp_http_client_get_status_code(client),
            esp_http_client_get_content_length(client));
    }else{
        ESP_LOGE(TAG5,"HTTP GET METHOD request failed: %s",esp_err_to_name(err));
    }
    esp_http_client_cleanup(client);

}

void createbody(char* name,char* gender,char* email,char* status,char *out){
    
    sprintf(out,
            "{"
            "  \"name\":\"TheName\", "
            "  \"gender\":\"TheGender\", "
            "  \"email\":\"TheEmail@u.com\", "
            "  \"status\":\"Active\" "
            "}"
            );

}

void onConnected(void *param){

    while (true)
    {
        if(xSemaphoreTake(onConnectionHandler,10*1000*portTICK_PERIOD_MS)){
            ESP_LOGI(TAG4,"Processing...");
            tcpip_adapter_ip_info_t ip_info;
            ESP_ERROR_CHECK(tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA,&ip_info));
            printf("IP Address: %s\n",ip4addr_ntoa(&ip_info.ip));
            printf("Mask Address: %s\n",ip4addr_ntoa(&ip_info.netmask));
            printf("IP Address: %s\n",ip4addr_ntoa(&ip_info.gw));
            printf("'Ολα γύρω σου γυρίζουν\n");
            printf("\n");
            struct FetchParams fparams;
            struct FetchParams postCall;

            fparams.onGotData = onGotData;
            fparams.method=GET;
            fparams.headerCount=0;
            fparams.body=NULL;
            

            fetch("http://quotes.rest/qod",&fparams);
            printf("get status %d: \n",fparams.status);
            
            if(fparams.status==429 || fparams.status==200){
                
                postCall.onGotData = onGotData;
                postCall.method=POST;
                
                
                Headers contentType={
                    .key = "Content-Type",
                    .val = "application/json"
                };
                Headers accept={
                    .key = "Accept",
                    .val = "application/json"
                };
                Headers auth={
                    .key = "Authorization",
                    .val = token
                };
                postCall.header[0]=contentType;
                postCall.header[1]=accept;
                postCall.header[2]=auth;
                postCall.headerCount=3;
                char buffer[1024];
                createbody("Thename","TheMale","TheEmail@mymail.com","Active",buffer);

                postCall.body=buffer;
                fetch("https://gorest.co.in/public/v1/users",&postCall);
            }
            ESP_LOGI(TAG7,"%s",fparams.message);
            ESP_LOGI(TAG8,"%s",postCall.message);
            ESP_LOGI(TAG4,"Finished Processing...");
            esp_wifi_disconnect();
            xSemaphoreTake(onConnectionHandler,portMAX_DELAY);
        }else{
            ESP_LOGE("Connection","Can not connect...");
            for (int i = 5; i >=0; i--)
            {
                ESP_LOGE(TAG4,"...%d",i);
                vTaskDelay(pdMS_TO_TICKS(1000));
            }
            esp_restart();
        }
    }
    
}

void runConnect(){

    nvs_flash_init();
    ESP_ERROR_CHECK(esp_netif_init());
    esp_event_loop_create_default();
    example_connect();

    esp_http_client_config_t client_config={
        .url="http://google.com",
        .event_handler=clientEvent
    };

    esp_http_client_handle_t client=esp_http_client_init(&client_config);
    esp_http_client_perform(client);
    esp_http_client_cleanup(client);

}

void wifiScanner(){
    wifiInit();

    wifi_scan_config_t scan_config={
        .ssid=0,
        .bssid=0,
        .channel=0,
        .show_hidden=true,
        .scan_type=WIFI_SCAN_TYPE_ACTIVE
    };

    ESP_ERROR_CHECK(esp_wifi_scan_start(&scan_config,true));

    wifi_ap_record_t wifi_records[MAX_APs];
    
    uint16_t maxRecords = MAX_APs;
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&maxRecords,wifi_records));

    printf("Found %d access points:\n", maxRecords);
    printf("\n");
    printf("            SSID                 | CHANNEL | RSSI |    Auth Mode  \n");
    printf("------------------------------------------------------------------\n");
    for(int i=0;i<maxRecords;i++){
        printf("%32s | %7d | %4d | %12s\n",(char *)wifi_records[i].ssid,wifi_records[i].primary,wifi_records[i].rssi,getAuthenticationModeName(wifi_records[i].authmode));
    }
}


void ntpTime(){

    time_t now=0;
    time(&now);
    print_time(now,"Time at the beginning:");

    nvs_flash_init();
    ESP_ERROR_CHECK(esp_netif_init());
    esp_event_loop_create_default();
    example_connect();

    sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
    sntp_setservername(0,"pool.ntp.org");
    sntp_init();
    sntp_set_time_sync_notification_cb(on_got_time);
}


void wifiConnectWithHandler(){
    
    onConnectionHandler = xSemaphoreCreateBinary();
    wifiInit();
    xTaskCreate(&onConnected,"on Connected",1024*6,NULL,5,NULL);
}